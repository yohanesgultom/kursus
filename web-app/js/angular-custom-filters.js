var m = angular.module('ngCustomFilters', []);

// regular OR filter for map
m.filter('mapfilter', function() {
    return function(input, query) {
        var res = [];
        var keys = [];
        if (query) {
            keys = Object.keys(query);
        } else {
            return input;
        }
        angular.forEach(input, function(o){
            var match = null;
            for (var i=0;i<keys.length;i++) {
                var test = null;
                if (!query[keys[i]]) {
                    test = true;
                } else if (query[keys[i]] == 'true' || query[keys[i]] == 'false') {
                    test = (o[keys[i]].toString() == query[keys[i]].toString());
                } else {
                    test = (o[keys[i]].toLowerCase().indexOf(query[keys[i]].toLowerCase()) !== -1);
                }
                match = (match == null) ? test :  match || test;
            }
            if (match) res.push(o);
        });
        return res;
    }
});

// an AND filter for map
m.filter('andmapfilter', function() {
    return function(input, query) {
        var res = [];
        var keys = [];
        if (query) {
            keys = Object.keys(query);
        } else {
            return input;
        }
        angular.forEach(input, function(o){
            var match = null;
            for (var i=0;i<keys.length;i++) {
                var test = null;
                if (!query[keys[i]]) {
                    test = true;
                } else if (query[keys[i]] == 'true' || query[keys[i]] == 'false') {
                    test = (o[keys[i]].toString() == query[keys[i]].toString());
                } else {
                    test = (o[keys[i]].toLowerCase().indexOf(query[keys[i]].toLowerCase()) !== -1);
                }
                match = (match == null) ? test :  match && test;
            }
            if (match) res.push(o);
        });
        return res;
    }
});
