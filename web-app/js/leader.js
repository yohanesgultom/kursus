var app = angular.module('leaderApp', ['ngRoute']);

app.config(function($httpProvider, $routeProvider) {
    // important! to make sure server receive the params
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    }

    // routes
    $routeProvider
    .when('/dashboard', {
        templateUrl: 'partialsDashboard',
        controller: 'dashboardController'
    })
    .otherwise({redirectTo: '/dashboard'});
});

app.controller('dashboardController', function($scope, $http) {

    $scope.loadMembers = function() {
        $http.get(contextPath + '/leader/loadMembers').success(function(res) {
            if (res) {
                if (res.success == true) {
                    $scope.members = res.members;
                } else {
                    alert(res.message);
                }
            } else {
                alert('Network failure. Please try to refresh')
            }
        });
    }

    $scope.openModule = function(id) {
        window.location = contextPath + '/member/module/' + id;
    }

    // init
    $scope.loadMembers();
});
