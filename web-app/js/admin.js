var app = angular.module('adminApp', ['ngRoute','ngSanitize','textAngular','ngCustomFilters']);

app.config(function($httpProvider, $routeProvider) {
    // important! to make sure server receive the params
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    }

    // routes
    $routeProvider
    .when('/dashboard', {templateUrl: 'partialsDashboard',controller: 'dashboardController'})
    .when('/module', {templateUrl: 'partialsModule',controller: 'moduleController'})
    .when('/group', {templateUrl: 'partialsGroup',controller: 'groupController'})
    .when('/user', {templateUrl: 'partialsUser',controller: 'userController'})
    .otherwise({redirectTo: '/dashboard'});
});

app.directive('ngBg', function(){
    return function(scope, element, attrs){
        var url = attrs.ngBg;
        element.css({
            'background-image': 'url(' + url +')',
            'background-size' : 'cover'
        });
    };
});

app.controller('dashboardController', function($scope, $http) {
    $scope.loadStatistics = function() {
        $scope.loading = true;
        $http.get(contextPath + '/admin/statistics').success(function(res) {
            if (res.success == true) {
                $scope.totalModules = res.totalModules;
                $scope.totalGroups = res.totalGroups;
                $scope.totalUsers = res.totalUsers;
            } else {
                var error = 'error';
                $scope.totalModules = error;
                $scope.totalGroups = error;
                $scope.totalUsers = error;
                alert(res.message);
            }
            $scope.loading = false;
        });
    }

    // init
    $scope.loadStatistics();
});

app.controller('moduleController', function($scope,$http) {

    $scope.moduleId = null;
    $scope.module = {};
    $scope.topic = {};

    $scope.loadModules = function() {
        $scope.loading = true;
        $http.get(contextPath + '/kursusModule/list').success(function(data) {
            $scope.modules = data.modules;
            $scope.loading = false;
        });
    }

    $scope.add = function() {
        $scope.module = null;
        $('#moduleModalLabel').text(addMessage);
        $('#moduleModal').modal('show');
    }
    $scope.edit = function(id) {
        $scope.module = $scope.modules[id];
        $('#moduleModalLabel').text(editMessage);
        $('#moduleModal').modal('show');
    }
    $scope.delete = function(id, name) {
        var msg = deleteMessage.replace('{0}', name);
        if(confirm(msg)){
            delete $scope.modules[id];
            $http.post(contextPath + '/kursusModule/delete', {id:id}).success(function(res){
                if (res) {
                    if (res.success != true) {
                        alert(res.message);
                    }
                } else {
                    alert("Network failure. Please retry");
                }
            });
        };
    }
    $scope.save = function() {
        var data = {id: $scope.module.id, name:$scope.module.name, description:$scope.module.description, cover:$scope.module.cover};
        // todo do proper validation
        if ($scope.module) {
            if ($scope.module.id) {
                // update
                $http.post(contextPath + '/kursusModule/update', data).success(function(res) {
                    if (res) {
                        if (res.success == true) {
                            $scope.modules[$scope.module.id] = $scope.module;
                            $('#moduleModal').modal('hide');
                        } else {
                            alert(res.message);
                        }
                    } else {
                        alert("Network failure. Please retry");
                    }
                });
            } else {
                // add
                $http.post(contextPath + '/kursusModule/save', data).success(function(res) {
                    if (res) {
                        if (res.success == true) {
                            $scope.module.id = res.id;
                            $scope.modules[$scope.module.id] = $scope.module;
                            $('#moduleModal').modal('hide');
                        } else {
                            alert(res.message);
                        }
                    } else {
                        alert("Network failure. Please retry");
                    }
                });
            }
        }
    }

    $scope.topicList = function(id,name) {
        $scope.moduleId = id;
        $scope.topicsLoading = true;
        $('#topicsModalLabel').text(name + ' Topics');
        $('#topicsModal').modal('show');
        $http.get(contextPath + '/topic/list',{params:{moduleId:id}}).success(function(data) {
            $scope.topics = data.topics;
            $scope.topicsLoading = false;
        });
    }

    $scope.topicAdd = function() {
        $scope.topic = null;
        $('#topicEditLabel').text("Add New Topic");
        $('#topicsModal').modal('hide');
        $('#topicEditModal').modal('show');
    }

    $scope.topicEdit = function(id) {
        $scope.topic = $scope.topics[id];
        $('#topicEditLabel').text("Edit Topic");
        $('#topicsModal').modal('hide');
        $('#topicEditModal').modal('show');
    }

    $scope.topicClose = function() {
        $scope.topic = null;
        $('#topicEditModal').modal('hide');
        $('#topicsModal').modal('show');
    }

    $scope.topicDelete = function(id, name) {
        if(confirm(deleteMessage + " " + name + " ? ")){
            delete $scope.topics[id];
            $http.post(contextPath + '/topic/delete', {id:id}).success(function(res){
                if (res) {
                    if (res.success != true) {
                        alert(res.message);
                    }
                } else {
                    alert("Network failure. Please retry");
                }
            });
        };
    }

    $scope.topicSave = function() {
        var data = {moduleId:$scope.moduleId, id:$scope.topic.id, title:$scope.topic.title, body:$scope.topic.body};
        // todo do proper validation
        if ($scope.topic) {
            if (data.id) {
                // update
                $http.post(contextPath + '/topic/update', data).success(function(res) {
                    if (res) {
                        if (res.success == true) {
                            $scope.topicClose();
                        } else {
                            alert(res.message);
                        }
                    } else {
                        alert("Network failure. Please retry");
                    }
                });
            } else {
                // add
                $http.post(contextPath + '/topic/save', data).success(function(res) {
                    if (res) {
                        if (res.success == true) {
                            $scope.topic.id = res.id;
                            $scope.topics[$scope.topic.id] = $scope.topic;
                            $scope.topicClose();
                        } else {
                            alert(res.message);
                        }
                    } else {
                        alert("Network failure. Please retry");
                    }
                });
            }
        }
    }

    // init
    $scope.loadModules();
});

app.controller('groupController', function($scope, $http) {

    var groupEditModal = $('#groupEditModal');
    var groupListModal = $('#groupListModal');

    $scope.disabled = false;

    $scope.loadGroups = function() {
        $scope.loading = true;
        $http.get(contextPath + '/kursusUserGroup/list').success(function(data) {
            $scope.groups = data.groups;
            $scope.loading = false;
        });
    }

    $scope.groupAdd = function() {
        $scope.group = null;
        $scope.groupEditModalLabel = addMessage;
        groupEditModal.modal('show');
    }
    $scope.groupEdit = function(id) {
        $scope.group = $scope.groups[id];
        $scope.groupEditModalLabel = editMessage;
        groupEditModal.modal('show');
    }
    $scope.groupDelete = function(id, name) {
        var msg = deleteMessage.replace('{0}', name);
        if(confirm(msg)){
            delete $scope.groups[id];
            $http.post(contextPath + '/kursusUserGroup/delete', {id:id}).success(function(res){
                if (res) {
                    if (res.success != true) {
                        alert(res.message);
                    }
                } else {
                    alert("Network failure. Please retry");
                }
            });
        };
    }
    $scope.groupSave = function() {
        var data = {id: $scope.group.id, name:$scope.group.name};
        // todo do proper validation
        if ($scope.group) {
            if ($scope.group.id) {
                // update
                $http.post(contextPath + '/kursusUserGroup/update', data).success(function(res) {
                    if (res) {
                        if (res.success == true) {
                            $scope.groups[$scope.group.id] = $scope.group;
                            groupEditModal.modal('hide');
                        } else {
                            alert(res.message);
                        }
                    } else {
                        alert("Network failure. Please retry");
                    }
                });
            } else {
                // add
                $http.post(contextPath + '/kursusUserGroup/save', data).success(function(res) {
                    if (res) {
                        if (res.success == true) {
                            $scope.group.id = res.id;
                            $scope.groups[$scope.group.id] = $scope.group;
                            groupEditModal.modal('hide');
                        } else {
                            alert(res.message);
                        }
                    } else {
                        alert("Network failure. Please retry");
                    }
                });
            }
        }
    }

    $scope.groupMembers = function(id) {
        $scope.newMember = null;
        $scope.groupListLoading = true;
        $http.get(contextPath + '/kursusUserGroup/members',{params:{groupId:id}}).success(function(res) {
            if (res.success == true) {
                $scope.selectedGroupId = id;
                $scope.members = res.members;
                groupListModal.modal('show');
            } else {
                alert(res.message);
            }
            $scope.groupListLoading = false;
        });
    }

    $scope.memberDelete = function(id,name) {
        if(confirm(deleteMessage + " " + name + " ? ")){
            delete $scope.members[id];
            $http.post(contextPath + '/kursusUserGroup/memberDelete', {userId:id, groupId:$scope.selectedGroupId}).success(function(res){
                if (res) {
                    if (res.success != true) {
                        alert(res.message);
                    }
                } else {
                    alert("Network failure. Please retry");
                }
            });
        };
    }

    $scope.memberAdd = function() {
        $scope.disabled = true;
        $http.post(contextPath + '/kursusUserGroup/memberAdd', {username:$scope.newMember, groupId:$scope.selectedGroupId}).success(function(res){
            if (res) {
                if (res.success != true) {
                    alert(res.message);
                } else {
                    $scope.members[res.member.id] = res.member;
                }
            } else {
                alert("Network failure. Please retry");
            }
            $scope.disabled = false;
        });
    }

    $scope.setLeader = function(id, name) {
        var msg = setLeaderMessage.replace('{0}',name);
        if (confirm(msg)) {
            $scope.members[id].roles[0] = 'leader';
            $http.post(contextPath + '/kursusUserGroup/setLeader', {userId:id}).success(function(res){
                if (res) {
                    if (res.success != true) {
                        alert(res.message);
                    }
                } else {
                    alert("Network failure. Please retry");
                }
            });
        }
    }

    $scope.removeLeader = function(id, name) {
        var msg = removeLeaderMessage.replace('{0}',name);
        if (confirm(msg)) {
            $scope.members[id].roles[0] = 'member';
            $http.post(contextPath + '/kursusUserGroup/removeLeader', {userId:id}).success(function(res){
                if (res) {
                    if (res.success != true) {
                        alert(res.message);
                    }
                } else {
                    alert("Network failure. Please retry");
                }
            });
        }
    }

    // init
    $scope.loadGroups();
});

app.controller('userController', function($scope, $http) {

    var userEditModal = $('#userEditModal');
    $scope.genders = {male:'M',female:'F'};
    $scope.user = {};

    $scope.loadUsers = function() {
        $scope.loading = true;
        $http.get(contextPath + '/kursusUser/list').success(function(res) {
            if (res.success == true) {
                $scope.users = res.users;
            } else {
                alert(res.message);
            }
            $scope.loading = false;
        });
    }

    $scope.userAdd = function() {
        $scope.user = {};
        $scope.userEditModalLabel = addMessage;
        $scope.user.gender = $scope.genders.male;
        $scope.user.role = 'member';
        userEditModal.modal('show');
    }

    $scope.userEdit = function(id) {
        $scope.userEditModalLabel = editMessage;
        $scope.user = $scope.users[id];
        userEditModal.modal('show');
    }

    $scope.userDelete = function(id, name) {
        $scope.user = $scope.users[id];
        var msg = deleteMessage.replace('{0}', name);
        if (confirm(msg)) {
            delete $scope.users[id];
            $http.post(contextPath + '/kursusUser/delete', $scope.user).success(function(res){
                if (res) {
                    if (res.success != true) {
                        alert(res.message);
                    }
                } else {
                    alert("Network failure. Please retry");
                }
            });
        }
    }

    $scope.userSave = function() {
        var valid = yvalidator.validate([
            {
                valid: ($scope.user.id || $scope.user.username),
                target: '#userEmail',
                message: 'Please provide email'
            },
            {
                valid: (!$scope.user.username || yvalidator.default.email($scope.user.username)),
                target: '#userEmail',
                message: 'Invalid email'
            },
            {
                valid: ($scope.user.fullname),
                target: '#userFullName',
                message: 'Please provide full name'
            },
            {
                valid: ($scope.user.id || $scope.user.password),
                target: '#userPassword',
                message: 'Please provide password'
            },
            {
                valid: ($scope.user.id || !$scope.user.password || $scope.user.password.length >= 6),
                target: '#userPassword',
                message: 'Password minimum length is 6'
            },
            {
                valid: ($scope.user.id || $scope.user.password == $scope.user.confirmpassword),
                target: '#userConfirmPassword',
                message: 'Password is not the same'
            }
        ]);

        if (valid) {
            if ($scope.user.id) {
                // update
                $http.post(contextPath + '/kursusUser/update', $scope.user).success(function(res) {
                    if (res) {
                        if (res.success == true) {
                            $scope.users[$scope.user.id] = $scope.user;
                            userEditModal.modal('hide');
                        } else {
                            alert(res.message);
                        }
                    } else {
                        alert("Network failure. Please retry");
                    }
                });
            } else {
                // add
                $http.post(contextPath + '/kursusUser/save', $scope.user).success(function(res) {
                    if (res) {
                        if (res.success == true) {
                            $scope.user.id = res.id;
                            $scope.users[$scope.user.id] = $scope.user;
                            userEditModal.modal('hide');
                        } else {
                            alert(res.message);
                        }
                    } else {
                        alert("Network failure. Please retry");
                    }
                });
            }
        }
    }
    // init
    $scope.loadUsers();

});
