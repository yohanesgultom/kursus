# Kursus #

### Demo ###

* [Online Bible Course](http://kursus.herokuapp.com) (take sometime to load due to Heroku sleep mode. If you see Application Error just refresh the page)
* Demo accounts: user@gmail.com/password, admin@gmail.com/password

### Description ###

* Kursus (Indonesian language for Course) is a simple mobile-friendly web-based online course provider. Each course's Module consist of question (Topic) that need to be answered. User(s) will be assigned to a group with one/more Leader that will monitor each group member's progress. 

* Kursus is built using [Groovy & Grails](grails.org), [PostgreSQL](postgresql.org), [Twitter Bootstrap](getbootstrap.com), [Angular.js](angularjs.org) and plenty of plugins (Shiro, OAuth, yValidator .etc)

* An example of usage of Kursus is [Online Bible Course](http://kursus.herokuapp.com), deployed in [Heroku](http://heroku.com) a wonderful-free cloud platform.

* Check out the wiki for user guide <https://bitbucket.org/yohanesgultom/kursus/wiki>

### How do I get set up? ###

* Download & setup Grails 2.1.1 (or higher) [Guide](http://grails.org/doc/2.1.1/guide/gettingStarted.html)
* Download & install PostgreSQL 9.0 (or higher) [Guide](http://www.postgresql.org/docs/9.0/interactive/installation.html)
* Create a database named `kursus`, set the username/password: `postgres/postgres` and run it at port `5432` (you can modify the configuration later in `config/Datasource.groovy`)
* Clone this repo and run grails from console `kursus/grails run-app`
* Access the app from browser (with HTML5 support) `http://localhost:8080/kursus`

### Note on contribution ###

* In order to deploy smoothly to Heroku, I prefer to stick on Grails 2.1.1 for now