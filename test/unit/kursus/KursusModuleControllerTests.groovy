package kursus



import org.junit.*
import grails.test.mixin.*

@TestFor(KursusModuleController)
@Mock(KursusModule)
class KursusModuleControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/kursusModule/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.kursusModuleInstanceList.size() == 0
        assert model.kursusModuleInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.kursusModuleInstance != null
    }

    void testSave() {
        controller.save()

        assert model.kursusModuleInstance != null
        assert view == '/kursusModule/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/kursusModule/show/1'
        assert controller.flash.message != null
        assert KursusModule.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusModule/list'

        populateValidParams(params)
        def kursusModule = new KursusModule(params)

        assert kursusModule.save() != null

        params.id = kursusModule.id

        def model = controller.show()

        assert model.kursusModuleInstance == kursusModule
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusModule/list'

        populateValidParams(params)
        def kursusModule = new KursusModule(params)

        assert kursusModule.save() != null

        params.id = kursusModule.id

        def model = controller.edit()

        assert model.kursusModuleInstance == kursusModule
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusModule/list'

        response.reset()

        populateValidParams(params)
        def kursusModule = new KursusModule(params)

        assert kursusModule.save() != null

        // test invalid parameters in update
        params.id = kursusModule.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/kursusModule/edit"
        assert model.kursusModuleInstance != null

        kursusModule.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/kursusModule/show/$kursusModule.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        kursusModule.clearErrors()

        populateValidParams(params)
        params.id = kursusModule.id
        params.version = -1
        controller.update()

        assert view == "/kursusModule/edit"
        assert model.kursusModuleInstance != null
        assert model.kursusModuleInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/kursusModule/list'

        response.reset()

        populateValidParams(params)
        def kursusModule = new KursusModule(params)

        assert kursusModule.save() != null
        assert KursusModule.count() == 1

        params.id = kursusModule.id

        controller.delete()

        assert KursusModule.count() == 0
        assert KursusModule.get(kursusModule.id) == null
        assert response.redirectedUrl == '/kursusModule/list'
    }
}
