package kursus



import org.junit.*
import grails.test.mixin.*

@TestFor(KursusRoleController)
@Mock(KursusRole)
class KursusRoleControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/kursusRole/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.kursusRoleInstanceList.size() == 0
        assert model.kursusRoleInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.kursusRoleInstance != null
    }

    void testSave() {
        controller.save()

        assert model.kursusRoleInstance != null
        assert view == '/kursusRole/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/kursusRole/show/1'
        assert controller.flash.message != null
        assert KursusRole.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusRole/list'

        populateValidParams(params)
        def kursusRole = new KursusRole(params)

        assert kursusRole.save() != null

        params.id = kursusRole.id

        def model = controller.show()

        assert model.kursusRoleInstance == kursusRole
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusRole/list'

        populateValidParams(params)
        def kursusRole = new KursusRole(params)

        assert kursusRole.save() != null

        params.id = kursusRole.id

        def model = controller.edit()

        assert model.kursusRoleInstance == kursusRole
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusRole/list'

        response.reset()

        populateValidParams(params)
        def kursusRole = new KursusRole(params)

        assert kursusRole.save() != null

        // test invalid parameters in update
        params.id = kursusRole.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/kursusRole/edit"
        assert model.kursusRoleInstance != null

        kursusRole.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/kursusRole/show/$kursusRole.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        kursusRole.clearErrors()

        populateValidParams(params)
        params.id = kursusRole.id
        params.version = -1
        controller.update()

        assert view == "/kursusRole/edit"
        assert model.kursusRoleInstance != null
        assert model.kursusRoleInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/kursusRole/list'

        response.reset()

        populateValidParams(params)
        def kursusRole = new KursusRole(params)

        assert kursusRole.save() != null
        assert KursusRole.count() == 1

        params.id = kursusRole.id

        controller.delete()

        assert KursusRole.count() == 0
        assert KursusRole.get(kursusRole.id) == null
        assert response.redirectedUrl == '/kursusRole/list'
    }
}
