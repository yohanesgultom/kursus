package kursus



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(KursusModule)
class KursusModuleTests {

    void testSomething() {
        fail "Implement me"
    }
}
