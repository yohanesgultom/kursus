package kursus



import org.junit.*
import grails.test.mixin.*

@TestFor(KursusUserController)
@Mock(KursusUser)
class KursusUserControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/kursusUser/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.kursusUserInstanceList.size() == 0
        assert model.kursusUserInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.kursusUserInstance != null
    }

    void testSave() {
        controller.save()

        assert model.kursusUserInstance != null
        assert view == '/kursusUser/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/kursusUser/show/1'
        assert controller.flash.message != null
        assert KursusUser.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusUser/list'

        populateValidParams(params)
        def kursusUser = new KursusUser(params)

        assert kursusUser.save() != null

        params.id = kursusUser.id

        def model = controller.show()

        assert model.kursusUserInstance == kursusUser
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusUser/list'

        populateValidParams(params)
        def kursusUser = new KursusUser(params)

        assert kursusUser.save() != null

        params.id = kursusUser.id

        def model = controller.edit()

        assert model.kursusUserInstance == kursusUser
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusUser/list'

        response.reset()

        populateValidParams(params)
        def kursusUser = new KursusUser(params)

        assert kursusUser.save() != null

        // test invalid parameters in update
        params.id = kursusUser.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/kursusUser/edit"
        assert model.kursusUserInstance != null

        kursusUser.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/kursusUser/show/$kursusUser.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        kursusUser.clearErrors()

        populateValidParams(params)
        params.id = kursusUser.id
        params.version = -1
        controller.update()

        assert view == "/kursusUser/edit"
        assert model.kursusUserInstance != null
        assert model.kursusUserInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/kursusUser/list'

        response.reset()

        populateValidParams(params)
        def kursusUser = new KursusUser(params)

        assert kursusUser.save() != null
        assert KursusUser.count() == 1

        params.id = kursusUser.id

        controller.delete()

        assert KursusUser.count() == 0
        assert KursusUser.get(kursusUser.id) == null
        assert response.redirectedUrl == '/kursusUser/list'
    }
}
