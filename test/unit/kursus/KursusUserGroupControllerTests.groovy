package kursus



import org.junit.*
import grails.test.mixin.*

@TestFor(KursusUserGroupController)
@Mock(KursusUserGroup)
class KursusUserGroupControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/kursusUserGroup/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.kursusUserGroupInstanceList.size() == 0
        assert model.kursusUserGroupInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.kursusUserGroupInstance != null
    }

    void testSave() {
        controller.save()

        assert model.kursusUserGroupInstance != null
        assert view == '/kursusUserGroup/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/kursusUserGroup/show/1'
        assert controller.flash.message != null
        assert KursusUserGroup.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusUserGroup/list'

        populateValidParams(params)
        def kursusUserGroup = new KursusUserGroup(params)

        assert kursusUserGroup.save() != null

        params.id = kursusUserGroup.id

        def model = controller.show()

        assert model.kursusUserGroupInstance == kursusUserGroup
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusUserGroup/list'

        populateValidParams(params)
        def kursusUserGroup = new KursusUserGroup(params)

        assert kursusUserGroup.save() != null

        params.id = kursusUserGroup.id

        def model = controller.edit()

        assert model.kursusUserGroupInstance == kursusUserGroup
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/kursusUserGroup/list'

        response.reset()

        populateValidParams(params)
        def kursusUserGroup = new KursusUserGroup(params)

        assert kursusUserGroup.save() != null

        // test invalid parameters in update
        params.id = kursusUserGroup.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/kursusUserGroup/edit"
        assert model.kursusUserGroupInstance != null

        kursusUserGroup.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/kursusUserGroup/show/$kursusUserGroup.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        kursusUserGroup.clearErrors()

        populateValidParams(params)
        params.id = kursusUserGroup.id
        params.version = -1
        controller.update()

        assert view == "/kursusUserGroup/edit"
        assert model.kursusUserGroupInstance != null
        assert model.kursusUserGroupInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/kursusUserGroup/list'

        response.reset()

        populateValidParams(params)
        def kursusUserGroup = new KursusUserGroup(params)

        assert kursusUserGroup.save() != null
        assert KursusUserGroup.count() == 1

        params.id = kursusUserGroup.id

        controller.delete()

        assert KursusUserGroup.count() == 0
        assert KursusUserGroup.get(kursusUserGroup.id) == null
        assert response.redirectedUrl == '/kursusUserGroup/list'
    }
}
