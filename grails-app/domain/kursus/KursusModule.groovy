package kursus

class KursusModule {
    String name
    String description
    String cover
    static hasMany = [topics:Topic, subscribers:KursusUser]
    static belongsTo = KursusUser
    static constraints = {
        name blank: false
        cover nullable: true
    }
}
