package kursus

class KursusUser {
    static GENDERS = [MALE:'M',FEMALE:'F']
    String username
    String fullname
    String gender
    String passwordHash
    String job
    String address
    String avatar
    KursusUserGroup kursusUserGroup

    static hasMany = [
            roles: KursusRole,
            permissions: String,
            subscriptions: KursusModule,
            responses: Response
    ]

    static constraints = {
        username nullable: false, blank: false, unique: true, email: true
        fullname blank: false
        gender nullable: true
        job nullable: true
        address nullable: true
        kursusUserGroup nullable: true
        avatar nullable: true
    }
}
