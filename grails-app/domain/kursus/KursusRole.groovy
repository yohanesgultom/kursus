package kursus

class KursusRole {
    static Roles = [Admin:"admin",Leader:"leader",Member:"member"]
    String name

    static hasMany = [users: KursusUser, permissions: String]
    static belongsTo = KursusUser

    static constraints = {
        name nullable: false, blank: false, unique: true
    }
}
