package kursus

class Response {
    String body
    static belongsTo = [user:KursusUser, topic:Topic]
    static constraints = {
        body blank: false
    }
    static mapping = {
        body type: 'text'
    }
}
