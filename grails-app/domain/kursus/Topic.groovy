package kursus

class Topic {
    String title
    String body
    static belongsTo = [kursusModule:KursusModule]
    static hasMany = [responses:Response]
    static constraints = {
        title blank: false
        body blank: false
        responses nullable: true
    }
    static mapping = {
        body type: 'text'
    }
}
