package kursus

class KursusUserGroup {
    String name
    static hasMany = [members:KursusUser]
    static constraints = {
        name blank: false
        members nullable: true
    }
}
