package kursus

import org.apache.shiro.SecurityUtils

class CommonService {

    KursusUser getCurrentUser() {
        def user = SecurityUtils.subject.session.getAttribute(KursusUser.class.name)
        if (!user) {
            def userId = SecurityUtils.subject.principals.oneByType(Long)
            user = KursusUser.findById(userId,[fetch:[kursusUserGroup:'eager']])
            SecurityUtils.subject.session.setAttribute(KursusUser.class.name, user)
        }
        return user
    }

    void updateCurrentUser(KursusUser user) {
        SecurityUtils.subject.session.setAttribute(KursusUser.class.name, user)
    }
}
