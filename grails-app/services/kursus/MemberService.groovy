package kursus

import grails.plugin.cache.CacheEvict
import grails.plugin.cache.Cacheable
import org.springframework.context.i18n.LocaleContextHolder

class MemberService {

    def messageSource

    def getUserModuleStat(KursusUser user, long moduleId) {
        def res = [topics: 0, responses: 0]
        def topics = Topic.executeQuery("select count(t.id) from Topic t where t.kursusModule.id = :id", [id:moduleId])
        def responses = Response.executeQuery("select count(r.id) from Response r join r.topic t join t.kursusModule km where km.id = :moduleId and r.user.id = :userId", [moduleId:moduleId, userId:user.id])
        res.topics = topics && !topics.isEmpty() && topics[0] ? topics[0] : 0;
        res.responses = responses && !responses.isEmpty() && responses[0] ? responses[0] : 0
        return res
    }

    @Cacheable(value='UserModules', key='#user.id')
    def getUserModules(KursusUser user) {
        def res = [modules: [:]]
        def modules = KursusModule.all
        List<Integer> subscriptions = KursusModule.executeQuery("select m.id from KursusModule m join m.subscribers s where s.id = :id", [id:user.id])
        modules?.each {
            def subscribed = subscriptions.contains(it.id)
            def map = [id: it.id, name: it.name, description: it.description, subscribed: subscribed, cover: it.cover, topics: 0, responses: 0, percentage: 0]
            if (subscribed) {
                def stat = getUserModuleStat(user, it.id)
                map.topics = stat.topics
                map.responses = stat.responses
                map.percentage = (stat.topics > 0 && stat.responses > 0) ? stat.responses / stat.topics * 100 : 0
            }
            res.modules[it.id] = map
        }
        return res
    }

    def getUserModules(long groupId) {
        def res = [:]
        def users = KursusUser.findAll("from KursusUser u where u.kursusUserGroup.id = ?",[groupId])
        users?.each {
            res[it.id] = [id:it.id, fullname:it.fullname, username: it.username, avatar:it.avatar, job:it.job, address:it.address, roles: it.roles*.name]
            res[it.id].modules = getUserModules(it).modules
        }
        return res
    }

    @CacheEvict(value=['UserModules'], key='#user.id')
    def addResponse(KursusUser user, String topicId, String body) {
        def response
        def message = messageSource.getMessage("message.responseaddsuccess", null, "Response successfully added", Locale.default)
        def res = [message: message, response: response]
        try {
            def topic = Topic.findById(topicId)
            response = new Response(body: body, topic: topic, user: user).save(failOnError: true)
            res.response = [id: response.id, body: response.body, userid: user.id, useravatar: user.avatar, username: user.fullname, self: true]
        } catch (Exception e) {
            log.error e.message, e
            message = e.message
        }
        return res
    }

    def updateResponse(KursusUser user, String topicId, String body) {
        def response
        def message = messageSource.getMessage("message.responseupdatesuccess", null, "Response successfully updated", Locale.default)
        try {
            response = Response.find("from Response where topic.id = :topicId and user.id = :userId", [topicId:Long.parseLong(topicId), userId:user.id])
            response.body = body
            response.save(failOnError: true)
        } catch (Exception e) {
            log.error e.message, e
            message = e.message
        }
        def res = [message: message, response: response]
        return res
    }

    @CacheEvict(value=['UserModules'], key='#user.id')
    def subscribeModule(KursusUser user, long moduleId) {
        def res = [message: "", success: true]
        try {
            user = KursusUser.find("from KursusUser ku left join fetch ku.subscriptions s where ku.id = :userId", [userId:user.id], [fetch: [subscriptions: "eager"]])
            def module = KursusModule.find("from KursusModule km where km.id = :moduleId", [moduleId:moduleId], [fetch: [subscribers: "eager"]])
            module.addToSubscribers(user)
            module.save(failOnError: true)
            res.message = messageSource.getMessage("kursus.message.subscibesuccess", null, "Module successfully subscribed", LocaleContextHolder.locale)
        } catch (Exception e) {
            log.error e.message, e
            res.message = messageSource.getMessage("kursus.message.subscibefailed", null, "Subscription failed", LocaleContextHolder.locale)
            res.success = false
        }
        return res
    }
}
