package kursus

import grails.converters.JSON

class KursusUserGroupController {

    static allowedMethods = [list:"GET", save: "POST", update: "POST", delete: "POST"]

    def list() {
        def res = [groups:[:]]
        KursusUserGroup.findAll().each {
            res.groups[it.id] = [id:it.id, name:it.name]
        }
        render res as JSON
    }

    def save() {
        def res = [success:true, message:null, id:null]
        try {
            def o = new KursusUserGroup(params)
            o.save(failOnError: true)
            res.id = o.id
        } catch (Exception e) {
            log.error e.message,e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def update() {
        def res = [success:true, message:null]
        try {
            def o = KursusUserGroup.findById(params.id)
            o.properties = params
            o.save(failOnError:true)
        } catch (Exception e) {
            log.error e.message,e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def delete() {
        def res = [success:true, message:null]
        try {
            def o = KursusUserGroup.findById(params.id)
            o.delete()
        } catch (Exception e) {
            log.error e.message,e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def members() {
        def res = [success: true, message: null, members:[:]]
        try {
            def l = KursusUser.findAll("from KursusUser u join fetch u.roles r where u.kursusUserGroup.id = ?", [params.long('groupId')])
            l?.each {
                res.members[it.id] = [id:it.id, fullname:it.fullname, username:it.username, roles: it.roles*.name]
            }
        } catch (Exception e) {
            log.error e.message,e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def memberDelete() {
        def res = [success: true, message: null]
        try {
            def u = KursusUser.findById(params.userId)
            u.kursusUserGroup = null
            u.save(failOnError: true)
        } catch (Exception e) {
            log.error e.message, e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def memberAdd() {
        def res = [success: true, message: null, member: null]
        try {
            def u = KursusUser.find("from KursusUser u join fetch u.roles r where u.username = ?", [params.username])
            if (!u) throw new Exception(g.message(code:"kursus.user.invalid",default: "Invalid user"))
            def g = KursusUserGroup.findById(params.groupId)
            if (!g) throw new Exception(g.message(code:"kursus.group.invalid",default: "Invalid group"))
            u.kursusUserGroup = g
            u.save(failOnError: true)
            res.member = u
        } catch (Exception e) {
            log.error e.message, e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def setLeader() {
        def res = [success: true, message: null]
        try {
            KursusUser.withTransaction {
                def u = KursusUser.findById(params.userId)
                def l = KursusRole.findByName(KursusRole.Roles.Leader)
                def m = KursusRole.findByName(KursusRole.Roles.Member)
                u.removeFromRoles(m)
                u.addToRoles(l)
                u.save(failOnError: true)
            }
        } catch (Exception e) {
            log.error e.message, e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def removeLeader() {
        def res = [success: true, message: null]
        try {
            KursusUser.withTransaction {
                def u = KursusUser.findById(params.userId)
                def l = KursusRole.findByName(KursusRole.Roles.Leader)
                def m = KursusRole.findByName(KursusRole.Roles.Member)
                u.removeFromRoles(l)
                u.addToRoles(m)
                u.save(failOnError: true)
            }
        } catch (Exception e) {
            log.error e.message, e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }
}
