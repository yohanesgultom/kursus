package kursus

import grails.converters.JSON
import grails.plugin.cache.CacheEvict

class TopicController {

    static allowedMethods = [list: "GET", save: "POST", update: "POST", delete: "POST"]

    def list() {
        def res = [topics:[:]]
        def moduleId = (params.moduleId) ? params.long("moduleId") : null
        def topics = Topic.findAll("from Topic where kursusModule.id = :moduleId", [moduleId:moduleId])
        topics?.each {
            res.topics[it.id] = it
        }
        render res as JSON
    }

    def save() {
        def res = [success:true, message:null, id:null]
        try {
            params.kursusModule = KursusModule.findById(params.moduleId)
            def o = new Topic(params)
            o.save(failOnError: true)
            res.id = o.id
        } catch (Exception e) {
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def update() {
        def res = [success:true, message:null]
        try {
            def o = Topic.findById(params.id)
            o.properties = params
            o.save(failOnError:true)
        } catch (Exception e) {
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def delete() {
        def res = [success:true, message:null]
        try {
            def o = Topic.findById(params.id)
            o.delete()
        } catch (Exception e) {
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

}
