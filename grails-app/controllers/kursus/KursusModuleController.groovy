package kursus

import grails.converters.JSON
import grails.plugin.cache.CacheEvict
import org.springframework.dao.DataIntegrityViolationException

class KursusModuleController {

    static allowedMethods = [list: "GET", save: "POST", update: "POST", delete: "POST"]

    def list() {
        def res = [modules:[:]]
        KursusModule.findAll().each {module ->
            res.modules[module.id] = [id:module.id, name:module.name, description:module.description, cover:module.cover]
        }
        render res as JSON
    }

    @CacheEvict(value = ['UserModules'], allEntries = true)
    def save() {
        def res = [success:true, message:null, id:null]
        def kursusModuleInstance
        try {
            kursusModuleInstance = new KursusModule(params).save(failOnError: true)
            res.id = kursusModuleInstance.id
        } catch (Exception e) {
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    @CacheEvict(value = ['UserModules'], allEntries = true)
    def update() {
        def res = [success:true, message:null]
        try {
            def kursusModuleInstance = KursusModule.findById(params.id)
            kursusModuleInstance.properties = params
            kursusModuleInstance.save(failOnError:true)
        } catch (Exception e) {
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    @CacheEvict(value = ['UserModules'], allEntries = true)
    def delete() {
        def res = [success:true, message:null]
        try {
            def kursusModuleInstance = KursusModule.findById(params.id)
            kursusModuleInstance.delete()
        } catch (Exception e) {
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }
}
