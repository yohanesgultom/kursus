package kursus

import org.springframework.dao.DataIntegrityViolationException

class KursusRoleController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [kursusRoleInstanceList: KursusRole.list(params), kursusRoleInstanceTotal: KursusRole.count()]
    }

    def create() {
        [kursusRoleInstance: new KursusRole(params)]
    }

    def save() {
        def kursusRoleInstance = new KursusRole(params)
        if (!kursusRoleInstance.save(flush: true)) {
            render(view: "create", model: [kursusRoleInstance: kursusRoleInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'kursusRole.label', default: 'KursusRole'), kursusRoleInstance.id])
        redirect(action: "show", id: kursusRoleInstance.id)
    }

    def show(Long id) {
        def kursusRoleInstance = KursusRole.get(id)
        if (!kursusRoleInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'kursusRole.label', default: 'KursusRole'), id])
            redirect(action: "list")
            return
        }

        [kursusRoleInstance: kursusRoleInstance]
    }

    def edit(Long id) {
        def kursusRoleInstance = KursusRole.get(id)
        if (!kursusRoleInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'kursusRole.label', default: 'KursusRole'), id])
            redirect(action: "list")
            return
        }

        [kursusRoleInstance: kursusRoleInstance]
    }

    def update(Long id, Long version) {
        def kursusRoleInstance = KursusRole.get(id)
        if (!kursusRoleInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'kursusRole.label', default: 'KursusRole'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (kursusRoleInstance.version > version) {
                kursusRoleInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'kursusRole.label', default: 'KursusRole')] as Object[],
                        "Another user has updated this KursusRole while you were editing")
                render(view: "edit", model: [kursusRoleInstance: kursusRoleInstance])
                return
            }
        }

        kursusRoleInstance.properties = params

        if (!kursusRoleInstance.save(flush: true)) {
            render(view: "edit", model: [kursusRoleInstance: kursusRoleInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'kursusRole.label', default: 'KursusRole'), kursusRoleInstance.id])
        redirect(action: "show", id: kursusRoleInstance.id)
    }

    def delete(Long id) {
        def kursusRoleInstance = KursusRole.get(id)
        if (!kursusRoleInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'kursusRole.label', default: 'KursusRole'), id])
            redirect(action: "list")
            return
        }

        try {
            kursusRoleInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'kursusRole.label', default: 'KursusRole'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'kursusRole.label', default: 'KursusRole'), id])
            redirect(action: "show", id: id)
        }
    }
}
