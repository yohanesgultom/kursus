package kursus

import grails.converters.JSON

class LeaderController {

    def commonService
    def memberService

    def index() {dashboard()}
    def dashboard() {}
    def partialsDashboard() {}

    def loadMembers() {
        def res = [success:true, message:null, members:[:]]
        try {
            if (commonService.currentUser.kursusUserGroup?.id) {
                res.members = memberService.getUserModules(commonService.currentUser.kursusUserGroup.id)
            }
        } catch (Exception e) {
            log.error e.message, e
            res.message = e.message
            res.success = false
        }
        render res as JSON
    }
}
