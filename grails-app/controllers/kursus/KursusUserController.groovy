package kursus

import grails.converters.JSON
import org.apache.shiro.crypto.hash.Sha256Hash

class KursusUserController {

    def commonService

    static allowedMethods = [list:"GET", save: "POST", update: "POST", delete: "POST"]

    def list() {
        def res = [success:true, message:null, users:[:]]
        try {
            KursusUser.findAll().each {
                res.users[it.id] = [id:it.id, fullname:it.fullname, username:it.username, gender:it.gender, role:it.roles?.asList()[0].name, avatar:it.avatar, job:it.job, address:it.address, self:(it.id.equals(commonService.currentUser.id))]
            }
        } catch (Exception e) {
            log.error e.message, e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def save() {
        def res = [success:true, message:null, id:null]
        try {
            params.passwordHash = new Sha256Hash(params.password).toHex()
            def o = new KursusUser(params)
            def r = KursusRole.findByName(params.role)
            o.addToRoles(r)
            o.save(failOnError: true)
            res.id = o.id
        } catch (Exception e) {
            log.error e.message,e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def update() {
        def res = [success:true, message:null]
        try {
            def o = KursusUser.findById(params.id)
            def r = KursusRole.findByName(params.role)
            o.properties = params
            if (!o.roles.contains(r)) {
                o.roles.clear()
                o.addToRoles(r)
            }
            o.save(failOnError:true)
        } catch (Exception e) {
            log.error e.message,e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def delete() {
        def res = [success:true, message:null]
        try {
            def o = KursusUser.findById(params.id)
            o.delete()
        } catch (Exception e) {
            log.error e.message,e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

}
