package kursus

import grails.converters.JSON
import org.apache.shiro.SecurityUtils
import org.apache.shiro.crypto.hash.Sha256Hash

class MemberController {

    def memberService
    def commonService

    def index = {
        redirect action: home
    }

    def home = {
        def user = commonService.currentUser
        [user:user]
    }

    def module = {
        def module = KursusModule.findById(params.id)
        [module:module]
    }

    def loadProfile = {
        def res = [success:true, message:null, user:null]
        try {
            def u = commonService.currentUser
            res.user = [id:u.id, username:u.username, fullname:u.fullname, avatar:u.avatar, address:u.address, job:u.job, gender:u.gender]
        } catch (Exception e) {
            log.error e.message, e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def saveProfile  = {
        def res = [success:true, message:null, user:null]
        try {
            def u = KursusUser.findById(commonService.currentUser.id)
            u.properties = params
            u.save(failOnError: true)
            commonService.updateCurrentUser(u)
            res.user = [id:u.id, username:u.username, fullname:u.fullname, avatar:u.avatar, address:u.address, job:u.job, gender:u.gender]
        } catch (Exception e) {
            log.error e.message, e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }

    def changePassword = {
        def res = [success:true, message:null, user:null]
        try {
            def u = KursusUser.findById(commonService.currentUser.id)
            def c = new Sha256Hash(params.current).toHex()
            def n = new Sha256Hash(params.new).toHex()
            if (!u.passwordHash.equals(c)) throw new Exception(g.message(code:"password.currentwrong.message", default:"Wrong Current Password"))
            u.passwordHash = n
            u.save(failOnError: true)
            commonService.updateCurrentUser(u)
            res.user = [id:u.id, username:u.username, fullname:u.fullname, avatar:u.avatar, address:u.address, job:u.job, gender:u.gender]
        } catch (Exception e) {
            log.error e.message, e
            res.success = false
            res.message = e.message
        }
        render res as JSON
    }


    def subscribeModule = {
        def user = commonService.currentUser
        def res = memberService.subscribeModule(user, params.long('moduleId'))
        render res as JSON
    }

    def moduleTopics = {
        def user = commonService.currentUser
        def topics = Topic.findAll("from Topic t where t.kursusModule.id = :moduleId", [moduleId:params.long('id')])
        def responses = []
        if (user.kursusUserGroup?.id) {
            // filter by group id
            responses = Response.findAll("from Response r join fetch r.topic t join fetch r.user u where t.kursusModule.id = :moduleId and u.kursusUserGroup.id = :groupId",[moduleId:params.long('id'), groupId:user.kursusUserGroup.id])
        } else {
            // filter by user id
            responses = Response.findAll("from Response r join fetch r.topic t join fetch r.user u where t.kursusModule.id = :moduleId and r.user.id = :userId",[moduleId:params.long('id'), userId:user.id])
        }
        def res = [topics: [:]]
        topics?.each { topic ->
            def map = [id: topic.id, title: topic.title, body: topic.body, responses: [:], responded: false]
            responses?.each { response ->
                if (response.topic.id.equals(topic.id)) {
                    if (user.id.equals(response.user.id)) map.responded = true
                    map.responses[response.id] = [id: response.id, body: response.body, userid: response.user.id, username: response.user.fullname, self: user.id.equals(response.user.id), useravatar: response.user.avatar]
                }
            }
            res.topics[topic.id] = map
        }
        render res as JSON
    }

    def topicResponses = {
        def responses = Response.findAll("from Response where topics.id = :id", [id:params.id])
        def res = [responses: responses]
        render res as JSON
    }

    def userModules = {
        def user = commonService.currentUser
        def res = memberService.getUserModules(user)
        render res as JSON
    }

    def userModuleStat = {
        def res = memberService.getUserModuleStat(params.long('id'))
        render res as JSON
    }

    def addResponse = {
        def user = commonService.currentUser
        def res = memberService.addResponse(user, params.topicId, params.body)
        render res as JSON
    }

    def updateResponse = {
        def user = commonService.currentUser
        def res = memberService.updateResponse(user, params.topicId, params.body)
        render res as JSON
    }

    // todo remove
    def sample = {}
}
