package kursus

import grails.converters.JSON
import grails.plugin.shiro.oauth.FacebookOAuthToken
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.crypto.hash.Sha256Hash
import org.apache.shiro.web.util.SavedRequest
import org.apache.shiro.web.util.WebUtils
import org.scribe.model.Token

class AuthController {
    def shiroSecurityManager
    def oauthService

    def index = { redirect(action: "login", params: params) }

    def login = {
        return [ username: params.username, rememberMe: (params.rememberMe != null), targetUri: params.targetUri ]
    }

    def signIn = {
        def authToken = new UsernamePasswordToken(params.username, params.password as String)

        // Support for "remember me"
        if (params.rememberMe) {
            authToken.rememberMe = true
        }
        
        // If a controller redirected to this page, redirect back
        // to it. Otherwise redirect to the root URI.
        def targetUri = params.targetUri ?: "/"
        
        // Handle requests saved by Shiro filters.
        SavedRequest savedRequest = WebUtils.getSavedRequest(request)
        if (savedRequest) {
            targetUri = savedRequest.requestURI - request.contextPath
            if (savedRequest.queryString) targetUri = targetUri + '?' + savedRequest.queryString
        }
        
        try{
            // Perform the actual login. An AuthenticationException
            // will be thrown if the username is unrecognised or the
            // password is incorrect.
            SecurityUtils.subject.login(authToken)

            log.info "Redirecting to '${targetUri}'."
            redirect(uri: targetUri)
        }
        catch (AuthenticationException ex){
            // Authentication failed, so display the appropriate message
            // on the login page.
            log.info "Authentication failure for user '${params.username}'."
            flash.message = message(code: "login.failed")

            // Keep the username and "remember me" setting so that the
            // user doesn't have to enter them again.
            def m = [ username: params.username ]
            if (params.rememberMe) {
                m["rememberMe"] = true
            }

            // Remember the target URI too.
            if (params.targetUri) {
                m["targetUri"] = params.targetUri
            }

            // Now redirect back to the login page.
            redirect(action: "login", params: m)
        }
    }

    def signOut = {
        // Log the user out of the application.
        SecurityUtils.subject?.logout()
        webRequest.getCurrentRequest().session = null

        // For now, redirect back to the home page.
        redirect(uri: "/")
    }

    def unauthorized = {
        render "You do not have permission to access this page."
    }

    def linkAccount = {
        def cloudUserInfo
        def cloudResponse
        def user
        Token token
        String sessionKey
        def memberRole = KursusRole.findByName("member")
        def authToken = session["shiroAuthToken"]
        try {
            if (authToken instanceof FacebookOAuthToken) {
                sessionKey = oauthService.findSessionKeyForAccessToken('facebook')
                token = session[sessionKey]
                cloudResponse = oauthService.getFacebookResource(token, 'https://graph.facebook.com/me')
                cloudUserInfo = JSON.parse(cloudResponse.body)
                cloudResponse = oauthService.getFacebookResource(token, 'https://graph.facebook.com/me/picture?redirect=0&type=square&height=100&width=100')
                cloudUserInfo.picture = JSON.parse(cloudResponse.body)
                user = KursusUser.findByUsername(cloudUserInfo.email)
                if (user == null) {
                    user = new KursusUser()
                    user.username = cloudUserInfo.email
                    user.fullname = cloudUserInfo.name
                    user.passwordHash = new Sha256Hash(cloudUserInfo.id).toHex()
                    user.gender = ("male".equalsIgnoreCase(cloudUserInfo.gender)) ? KursusUser.GENDERS.MALE : ("female".equalsIgnoreCase(cloudUserInfo.gender)) ? KursusUser.GENDERS.FEMALE : null
                    user.avatar = cloudUserInfo.picture?.data?.url
                    //user.avatar = g.resource(dir: "images", file: "default_user.png")
                    //user.work = (cloudUserInfo.work && ((List)cloudUserInfo.work)[0] && ((List)cloudUserInfo.work)[0].employer) ? ((List)cloudUserInfo.work)[0].employer.name : null
                    user.addToRoles(memberRole)
                    user.save(flush: true)
                }
            }

            SecurityUtils.subject.login new UsernamePasswordToken(user.username, cloudUserInfo.id, false)
            forward controller: "shiroOAuth", action: "linkAccount", params: [targetUri: "/"]
        } catch (Exception e) {
            log.error(e.message)
            flash.error = message(code: "app.error.sso.message",default: "Social media login error") + ". " + e.message
            redirect(uri: "/")
        }
    }

    def register() {
        def res = [success:true, message:null]
        try {
            params.passwordHash = new Sha256Hash(params.password).toHex()
            params.avatar = g.resource(dir: "images", file: "default_user.png")
            def u = new KursusUser(params)
            def r = KursusRole.findByName("member")
            u.addToRoles(r)
            u.save(failOnError: true, flush: true)
            SecurityUtils.subject.login new UsernamePasswordToken(params.username, params.password as String)
        } catch (Exception e) {
            log.error e.message, e
            res.success = false
            if (e.message?.contains("unique")) {
                res.message = g.message(code:"emailexist", default: "Email is already registered")
            } else {
                res.message = e.message
            }
        }
        render res as JSON
    }
}
