package kursus

import grails.converters.JSON

class AdminController {
    def index() {dashboard()}
    def dashboard() {}
    def partialsDashboard() {}
    def partialsModule() {}
    def partialsGroup() {}
    def partialsUser() {}
    def statistics() {
        def res = [:]
        res.success = true
        res.message = null
        try {
            res.totalModules = KursusModule.count
            res.totalGroups = KursusUserGroup.count
            res.totalUsers = KursusUser.count
        } catch (Exception e) {
            log.error e.message
            res.message = e.message
            res.success = false
        }
        render res as JSON
    }
}
