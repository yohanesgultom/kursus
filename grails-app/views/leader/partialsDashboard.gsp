<style>
.module {
    cursor: pointer;
}

.starblue {
    width: 52px;
    height: 48px;
    background-image: url(${g.resource(dir: "images", file: "starblue.png")});
    background-size: cover;
}

.star {
    width: 52px;
    height: 48px;
    background-image: url(${g.resource(dir: "images", file: "stargold.png")});
    background-size: cover;
}

</style>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><g:message code="kursus.leader.dashboard" default="Leader Dashboard"></g:message></h1>
    </div>
</div>

<g:img file="ajax-loader.gif" ng-show="loading"></g:img>
<div class="row">
    <div class="col-lg-4" ng-repeat="member in members">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="media">
                    <a class="thumbnail pull-left" href="#">
                        <img ng-src="{{member.avatar}}" class="media-object" width="100px" height="100px"/>
                    </a>
                    <div class="media-body">
                        <p>{{member.fullname}}</p>
                        <p>
                            <span class="label label-info">{{member.job}}</span>
                            <span class="label label-primary">{{member.address}}</span>
                        </p>
                        <div class="starblue" ng-show="member.roles[0] == 'admin'" title="{{member.roles[0]}}"></div>
                        <div class="star" ng-show="member.roles[0] == 'leader'" title="{{member.roles[0]}}"></div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="module" ng-repeat="module in member.modules" ng-show="module.subscribed" ng-click="openModule(module.id)">
                    <p class="lead">{{module.name}}</p>
                    <div class="progress progress-striped">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{module.percentage}}" aria-valuemin="0" aria-valuemax="100" style="{{'width:' + module.percentage + '%'}}">
                            <span>{{module.percentage}}% <g:message code="complete" default="Complete"/></span>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
