<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="generator" content="Bootply" />
    <title><g:layoutTitle default="Page"/> | <g:message code="app.name" default="Online Bible Course"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="${resource(dir: 'bootstrap/css', file: 'bootply-theme.css')}" type="text/css">
    <g:layoutHead/>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0-beta.13/angular.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0-beta.13/angular-sanitize.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <g:javascript src="angular-custom-filters.js"></g:javascript>
    <g:javascript src="yvalidator.js"></g:javascript>
    <r:layoutResources />
</head>

<body ng-controller="controller">
<div class="navbar navbar-fixed-top header">
    <div class="col-md-12">
        <div class="navbar-header">
            <a href="${request.contextPath}/" class="navbar-brand"><g:message code="app.name" default="Online Bible Course"></g:message></a>
        </div>
    </div>
</div>

<div class="navbar navbar-default" id="subnav">
    <div class="col-md-12">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
                <span class="sr-only"><g:message code="kursus.togglenav" default="Toggle Navigation"/></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse2">
            <ul class="nav navbar-nav navbar-right">
                <g:if test="${org.apache.shiro.SecurityUtils.subject.isPermitted("admin:*")}">
                    <li><a href="${g.createLink(controller: 'admin', action: 'dashboard')}"><g:message code="kursus.admin.dashboard" default="Admin Dashboard"/></a></li>
                </g:if>
                <g:if test="${org.apache.shiro.SecurityUtils.subject.isPermitted("leader:*")}">
                    <li><a href="${g.createLink(controller: 'leader', action: 'dashboard')}"><g:message code="kursus.leader.dashboard" default="Leader Dashboard"/></a></li>
                </g:if>
                <li><a href="${g.createLink(controller: 'member', action: 'home')}"><g:message code="kursus.home" default="Home"/></a></li>
                <li><a href="${g.createLink(controller: 'auth', action: 'signOut')}"><g:message code="kursus.signout" default="Sign out"/></a></li>
            </ul>
        </div>
    </div>
</div>

<!--main-->
<div class="container" id="main">
    <!-- body starts here -->
    <g:layoutBody/>
    <!-- body ends here -->
    <br>
    <div class="clearfix"></div>
    <hr>
    <div class="col-md-12 text-center">
        <p>
            <a href="https://bitbucket.org/yohanesgultom/kursus" target="_ext"><g:message code="poweredby" default="Powered by"/> Kursus &copy; 2014</a>
        </p>
    </div>
    <hr>
</div>
</div><!--/main-->
<script>

    $(document).ready(function() {

        /* toggle layout */
        $('#btnToggle').click(function(){
            if ($(this).hasClass('on')) {
                $('#main .col-md-6').addClass('col-md-4').removeClass('col-md-6');
                $(this).removeClass('on');
            }
            else {
                $('#main .col-md-4').addClass('col-md-6').removeClass('col-md-4');
                $(this).addClass('on');
            }
        });

    });
</script>
<r:layoutResources />

</body>
</html>

