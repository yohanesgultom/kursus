<html>
<head>
    <meta name="layout" content="public">
    <title><g:message code="pagenotfound" default="Page not found"></g:message> | <g:message code="app.name" default="Online Bible Course"/></title>
    <r:style>
        body {
            text-align: center;
        }

        h3 {
            color: #aaa;
        }
    </r:style>
</head>
<body>
    <h1><g:message code="pagenotfoundmessage" default="Sorry, page not found. 404"/></h1>
    <h3>[insert funny message here]</h3>
</body>
</html>