<html>
<head>
    <meta name="layout" content="public">
    <title><g:message code="kursus.signin" default="Sign in"></g:message></title>
    <r:style>

    /*
        Note: It is best to use a less version of this file ( see http://css2less.cc
        For the media queries use @screen-sm-min instead of 768px.
        For .omb_spanOr use @body-bg instead of white.
    */

    @media (min-width: 768px) {
        .omb_row-sm-offset-3 div:first-child[class*="col-"] {
            margin-left: 25%;
        }
    }

    .omb_login .omb_title {
        text-align: center;
    }

    .omb_login .omb_authTitle {
        text-align: center;
        line-height: 300%;
        color: #aaa;
    }

    .omb_login .omb_socialButtons a {
        color: white; // In yourUse @body-bg
        opacity:0.9;
    }
    .omb_login .omb_socialButtons a:hover {
        color: white;
        opacity:1;
    }
    .omb_login .omb_socialButtons .omb_btn-facebook {background: #3b5998;}
    .omb_login .omb_socialButtons .omb_btn-linkedin {background: #00aced;}

    .omb_login .omb_loginOr {
        position: relative;
        font-size: 1.5em;
        color: #aaa;
        margin-top: 1em;
        margin-bottom: 1em;
        padding-top: 0.5em;
        padding-bottom: 0.5em;
    }
    .omb_login .omb_loginOr .omb_hrOr {
        background-color: #cdcdcd;
        height: 1px;
        margin-top: 0px !important;
        margin-bottom: 0px !important;
    }
    .omb_login .omb_loginOr .omb_spanOr {
        display: block;
        position: absolute;
        left: 50%;
        top: -0.6em;
        margin-left: -1.5em;
        background-color: white;
        width: 3em;
        text-align: center;
    }

    .omb_login .omb_loginForm .input-group.i {
        width: 2em;
    }
    .omb_login .omb_loginForm  .help-block {
        color: red;
    }


    @media (min-width: 768px) {
        .omb_login .omb_forgotPwd {
            text-align: right;
            margin-top:10px;
        }
    }
    </r:style>
</head>
<body>
<div class="omb_login">
    <h1 class="omb_title"><g:message code="app.name" default="Online Bible Course"/></h1>
    <br><br>
    <div class="row omb_row-sm-offset-3 omb_socialButtons">
        <div class="col-xs-12 col-sm-6">
            <oauth:connect provider="facebook" class="btn btn-lg btn-block omb_btn-facebook">
                <i class="fa fa-facebook visible-xs"></i>
                <span class="hidden-xs">Facebook</span>
            </oauth:connect>
        </div>
    </div>

    <div class="row omb_row-sm-offset-3 omb_loginOr">
        <div class="col-xs-12 col-sm-6">
            <hr class="omb_hrOr">
            <span class="omb_spanOr"><g:message code="or" default="or"/></span>
        </div>
    </div>

    <div class="row omb_row-sm-offset-3">
        <div class="col-xs-12 col-sm-6">
            <g:form action="signIn" class="omb_loginForm" autocomplete="off" method="POST">
                <input type="hidden" name="targetUri" value="${targetUri}"/>
                <g:if test="${flash.message}">
                    <div class="alert alert-warning">${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                    <div class="alert alert-danger">${flash.error}</div>
                </g:if>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" class="form-control" name="username" placeholder="email address">
                </div>
                <span class="help-block"></span>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <div class="input-group">
                    <label class="checkbox">
                        <input type="checkbox" name="rememberMe" value="true"><g:message code="rememberme" default="Remember me"/>
                    </label>
                </div>
                <span class="help-block"></span>
                <button class="btn btn-lg btn-primary btn-block" type="submit"><g:message code="signin" default="Sign in"/></button>
            </g:form>
        </div>
    </div>

    <div class="row omb_row-sm-offset-3">
        <div class="col-xs-12 col-sm-6">
            <button class="btn btn-lg btn-success btn-block" data-toggle="modal" data-target="#registerModal"><g:message code="register" default="Register"/></button>
        </div>
    </div>

</div>

<!-- Registration Modal -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="moduleModal" aria-hidden="true" ng-app="app">
    <div class="modal-dialog" ng-controller="controller">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><g:message code="Registration" default="Registration"/></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label id="userEmail"><g:message code="kursus.user.email" default="E-mail"/></label>
                    <input type="text" class="form-control" ng-model="user.email">
                </div>
                <div class="form-group">
                    <label id="userFullName"><g:message code="kursus.user.fullname" default="Full Name"/></label>
                    <input type="text" class="form-control" ng-model="user.fullname">
                </div>
                <div class="form-group">
                    <div class="radio-inline">
                        <label><input type="radio" name="gender" value="{{genders.male}}" ng-model="user.gender" checked> <g:message code="male" default="Male"/></label>
                    </div>
                    <div class="radio-inline">
                        <label><input type="radio" name="gender" value="{{genders.female}}" ng-model="user.gender"> <g:message code="female" default="Female"/></label>
                    </div>
                </div>
                <div class="form-group">
                    <label id="userPassword"><g:message code="kursus.user.password" default="Password"/></label>
                    <input type="password" class="form-control" ng-model="user.password">
                </div>
                <div class="form-group">
                    <label id="userConfirmPassword"><g:message code="kursus.user.confirmpassword" default="Confirm password"/></label>
                    <input type="password" class="form-control" ng-model="user.confirmpassword">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" ng-disabled="disabled"><g:message code="cancel" default="Cancel"/></button>
                <button type="button" class="btn btn-primary" ng-click="register()" ng-disabled="disabled"><g:message code="register" default="Register"/></button>
            </div>
        </div>
    </div>
</div>

<r:script>
var registrationModal = $('#registerModal');
var contextPath = '${request.contextPath}';

angular.module('app',[])
.config(function($httpProvider) {
    // important! to make sure server receive the params
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) { return data;}
        return $.param(data);
    }
})
.controller('controller', function($scope, $http) {
    $scope.disabled = false;
    $scope.genders = {male:'M',female:'F'};
    $scope.user = {gender:$scope.genders.male};
    $scope.register = function() {
        $scope.disabled = true;
        var valid = yvalidator.validate([
            {
                valid: ($scope.user.email),
                target: '#userEmail',
                message: 'Please provide email'
            },
            {
                valid: (!$scope.user.email || yvalidator.default.email($scope.user.email)),
                target: '#userEmail',
                message: 'Invalid email format'
            },
            {
                valid: ($scope.user.fullname),
                target: '#userFullName',
                message: 'Please provide full name'
            },
            {
                valid: ($scope.user.password),
                target: '#userPassword',
                message: 'Please provide password'
            },
            {
                valid: (!$scope.user.password || $scope.user.password.length >= 6),
                target: '#userPassword',
                message: 'Password minimum length is 6'
            },
            {
                valid: ($scope.user.password == $scope.user.confirmpassword),
                target: '#userConfirmPassword',
                message: 'Password is not the same'
            }
        ]);

        if (valid) {
            var data = {username:$scope.user.email, fullname:$scope.user.fullname, gender:$scope.user.gender, password:$scope.user.password};
            $http.post(contextPath + '/auth/register', data).success(function(res) {
                if (res.success == true) {
                    registrationModal.modal('hide');
                    window.location = contextPath;
                } else {
                    alert(res.message);
                }
                $scope.disabled = false;
            });
        } else {
            $scope.disabled = false;
        }
    }
});

</r:script>
</body>
</html>