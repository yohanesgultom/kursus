<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><g:message code="kursus.admin.users" default="Users"></g:message></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <span class="btn btn-success btn-lg" ng-click="userAdd()"><i class="glyphicon glyphicon-plus"></i> <g:message code="add" default="Add"></g:message></span>
        <br><br>
        <div class="form-group has-feedback">
            <input type="text" class="form-control input-lg" ng-model="userSearch" placeholder="${g.message(code:'kursus.user.searchplaceholder',default:'Search user')}">
            <span class="glyphicon glyphicon-search form-control-feedback" style="top:1px"></span>
        </div>
        <g:img file="ajax-loader.gif" ng-show="loading"></g:img>
        <table class="table table-bordered table-striped">
            <tr>
                <th><g:message code="kursus.user.fullname" default="Full Name"></g:message></th>
                <th><g:message code="kursus.user.username" default="Username"></g:message></th>
                <th><g:message code="kursus.user.role" default="Role"></g:message></th>
                <th><g:message code="action" default="Action"></g:message></th>
            </tr>
            <tr class="user" ng-repeat="user in users | mapfilter:{fullname:userSearch,username:userSearch}">
                <td>{{user.fullname}}</td>
                <td>{{user.username}}</td>
                <td>{{user.role}}</td>
                <td class="action">
                    <span class="btn btn-info" title="edit" ng-click="userEdit(user.id)">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </span>
                    <span class="btn btn-danger" title="delete" ng-click="userDelete(user.id,user.fullname)" ng-hide="user.self">
                        <i class="glyphicon glyphicon-remove"></i>
                    </span>
                </td>
            </tr>
        </table>
    </div>
</div>

<!-- user edit modal -->
<div class="modal fade" id="userEditModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="lead">{{userEditModalLabel}}</div>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label id="userEmail"><g:message code="kursus.user.email" default="E-mail"/></label>
                    <input type="text" class="form-control" ng-model="user.username" ng-disabled="user.id">
                </div>
                <div class="form-group">
                    <label id="userFullName"><g:message code="kursus.user.fullname" default="Full Name"/> * </label>
                    <input type="text" class="form-control" ng-model="user.fullname">
                </div>
                <div class="form-group" ng-hide="user.self">
                    <label id="userRole"><g:message code="kursus.user.role" default="Role"/> * </label>
                    <!-- todo make dynamic -->
                    <select class="form-control" ng-model="user.role">
                        <option value="member"><g:message code="kursus.user.role.member" default="Member"/></option>
                        <option value="leader"><g:message code="kursus.user.role.leader" default="Leader"/></option>
                        <option value="admin"><g:message code="kursus.user.role.admin" default="Admin"/></option>
                    </select>
                </div>
                <div class="form-group" ng-hide="user.id">
                    <label id="userPassword"><g:message code="kursus.user.password" default="Password"/> * </label>
                    <input type="password" class="form-control" ng-model="user.password" ng-disable="user.id">
                </div>
                <div class="form-group" ng-hide="user.id">
                    <label id="userConfirmPassword"><g:message code="kursus.user.confirmpassword" default="Confirm password"/> * </label>
                    <input type="password" class="form-control" ng-model="user.confirmpassword" ng-disable="user.id">
                </div>
                <div class="form-group">
                    <div class="radio-inline">
                        <label><input type="radio" name="gender" value="{{genders.male}}" ng-model="user.gender" checked> <g:message code="male" default="Male"/></label>
                    </div>
                    <div class="radio-inline">
                        <label><input type="radio" name="gender" value="{{genders.female}}" ng-model="user.gender"> <g:message code="female" default="Female"/></label>
                    </div>
                </div>
                <div class="form-group">
                    <label id="userAvatar"><g:message code="kursus.user.avatar" default="Avatar"/></label>
                    <input type="text" class="form-control" ng-model="user.avatar">
                </div>
                <div class="form-group">
                    <label id="userJob"><g:message code="kursus.user.job" default="Job"/></label>
                    <input type="text" class="form-control" ng-model="user.job">
                </div>
                <div class="form-group">
                    <label id="userAddress"><g:message code="kursus.user.address" default="Address"/></label>
                    <input type="text" class="form-control" ng-model="user.address">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" ng-disabled="disabled"><g:message code="default.button.cancel.label" default="Cancel"/></button>
                <button type="button" class="btn btn-primary" ng-click="userSave()" ng-disabled="disabled"><g:message code="default.button.save.label" default="Save"/></button>
            </div>
        </div>
    </div>
</div>
