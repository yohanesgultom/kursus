<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><g:message code="kursus.admin.dashboard" default="Admin Dashboard"></g:message></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> <g:message code="kursus.admin.dashboard.statistics" default="Statistics"></g:message>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <tr>
                        <td><i class="fa fa-book fa-fw"></i> <g:message code="kursus.admin.dashboard.statistics.totalmodules" default="Total Modules"></g:message></td>
                        <td><i class="fa fa-refresh fa-spin" ng-show="loading"/>{{totalModules}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-group fa-fw"></i> <g:message code="kursus.admin.dashboard.statistics.totalgroups" default="Total Groups"></g:message></td>
                        <td><i class="fa fa-refresh fa-spin" ng-show="loading"/>{{totalGroups}}</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-user fa-fw"></i> <g:message code="kursus.admin.dashboard.statistics.totalusers" default="Total Users"></g:message></td>
                        <td><i class="fa fa-refresh fa-spin" ng-show="loading"/>{{totalUsers}}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
