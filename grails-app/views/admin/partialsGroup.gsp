<style>
.starblue {
    width: 52px;
    height: 48px;
    background-image: url(${g.resource(dir: "images", file: "starblue.png")});
    background-size: cover;
}

.star {
    width: 52px;
    height: 48px;
    background-image: url(${g.resource(dir: "images", file: "stargold.png")});
    background-size: cover;
    cursor: pointer;
}

.starable {
    width: 52px;
    height: 48px;
    cursor: pointer;
}

.starable:hover {
    background-image: url(${g.resource(dir: "images", file: "stargold.png")});
    background-size: cover;
}
</style>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><g:message code="kursus.admin.group" default="Group"></g:message></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <span class="btn btn-success btn-lg" ng-click="groupAdd()"><i class="glyphicon glyphicon-plus"></i> <g:message code="add" default="Add"></g:message></span>
        <br><br>
        <table class="table table-bordered table-striped">
            <tr>
                <th><g:message code="kursus.group.name" default="Group Name"></g:message></th>
                <th><g:message code="action" default="Action"></g:message></th>
            </tr>
            <tr class="group" ng-repeat="group in groups | orderBy:'name'">
                <td>{{group.name}}</td>
                <td class="action">
                    <span class="btn btn-primary" title="members" ng-click="groupMembers(group.id)">
                        <i class="fa fa-group"></i>
                    </span>
                    <span class="btn btn-info" title="edit" ng-click="groupEdit(group.id)">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </span>
                    <span class="btn btn-danger" title="delete" ng-click="groupDelete(group.id,group.name)">
                        <i class="glyphicon glyphicon-remove"></i>
                    </span>
                </td>
            </tr>
        </table>
    </div>
</div>

<!-- group edit modal -->
<div class="modal fade" id="groupEditModal" tabindex="-1" role="dialog" aria-labelledby="moduleModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="groupEditLabel">{{groupEditModalLabel}}</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="groupId" ng-model="group.id">
                <div class="form-group">
                    <label><g:message code="kursus.group.name" default="Group Name"/></label>
                    <input type="text" class="form-control" ng-model="group.name">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal""><g:message code="cancel" default="Cancel"/> </button>
                <button type="button" class="btn btn-primary" ng-click="groupSave()"><g:message code="save" default="Save"/> </button>
            </div>
        </div>
    </div>
</div>

<!-- group list modal -->
<div class="modal fade" id="groupListModal" tabindex="-1" role="dialog" aria-labelledby="moduleModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="groupListLabel"><g:message code="kursus.group.members" default="Group Members"></g:message></h4>
            </div>
            <div class="modal-body">
                <div class="form-inline" role="form">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-8 col-lg-8">
                                <input type="text" class="form-control" ng-model="newMember">
                            </div>
                            <div class="col-xs-1 col-lg-1">
                                <span class="btn btn-success" ng-click="memberAdd()" ng-disabled="disabled"><i class="fa fa-plus"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <g:img file="ajax-loader.gif" ng-show="groupListLoading"></g:img>
                <table class="table table-striped">
                    <tr>
                        <th><g:message code="kursus.user.fullname" default="Name"></g:message></th>
                        <th><g:message code="kursus.user.username" default="Username"></g:message></th>
                        <th><g:message code="kursus.user.leader" default="Leader"></g:message></th>
                        <th> </th>
                    </tr>
                    <tr ng-repeat="member in members | orderBy:'fullname'">
                        <td>{{member.fullname}}</td>
                        <td>{{member.username}}</td>
                        <td>
                            <div class="starblue" ng-show="member.roles[0] == 'admin'"></div>
                            <div class="star" ng-show="member.roles[0] == 'leader'" ng-click="removeLeader(member.id, member.fullname)"></div>
                            <div class="starable" ng-show="member.roles[0] == 'member'" ng-click="setLeader(member.id, member.fullname)"></div>
                        </td>
                        <td><span class="btn btn-danger" title="delete" ng-click="memberDelete(member.id,member.fullname)"><i class="glyphicon glyphicon-remove"></i></span></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
