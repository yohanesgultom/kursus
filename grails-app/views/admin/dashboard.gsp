<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><g:message code="kursus.admin.dashboard" default="Admin Dashboard"></g:message> | <g:message code="app.name" default="Online Bible Course"/></title>
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="${resource(dir: 'bootstrap/css', file: 'sb-admin.css')}" type="text/css">
</head>
<body ng-app="adminApp">
<div id="wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">

        <!-- nav header -->
        <div class="navbar-header">
            <!-- dropdown menu on small screen -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- brand label -->
            <a class="navbar-brand" href="${request.contextPath}/">
            <g:message code="app.name" default="Online Bible Course"/>
            </a>
        </div>

        <!-- user menu dropdown -->
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <!-- on extra small screen -->
                        <a class="visible-xs pull-right" href="${g.createLink(controller: 'auth', action: 'signOut')}"><i class="fa fa-sign-out fa-fw"></i></a>
                        <!-- on other screen -->
                        <a class="hidden-xs" href="${g.createLink(controller: 'auth', action: 'signOut')}"><i class="fa fa-sign-out fa-fw"></i> <g:message code="kursus.signout" default="Sign out"/></a>
                    </li>
                </ul>
            </li>
        </ul>

        <div class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li id="nav-dashboard">
                        <a href="#dashboard"><i class="fa fa-dashboard fa-fw"></i> <g:message code="kursus.admin.dashboard.index" default="Dashboard"></g:message></a>
                    </li>
                    <li id="nav-module">
                        <a href="#module"><i class="fa fa-edit fa-fw"></i> <g:message code="kursus.admin.module" default="Module"></g:message></a>
                    </li>
                    <li id="nav-group">
                        <a href="#group"><i class="fa fa-group fa-fw"></i> <g:message code="kursus.admin.group" default="Group"></g:message></a>
                    </li>
                    <li id="nav-user">
                        <a href="#user"><i class="fa fa-user fa-fw"></i> <g:message code="kursus.admin.user" default="User"></g:message></a>
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <div ng-view>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><g:message code="kursus.admin.dashboard" default="Dashboard"></g:message></h1>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0-beta.13/angular.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0-beta.13/angular-route.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0-beta.13/angular-sanitize.min.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<g:javascript src="angular-custom-filters.js"></g:javascript>
<g:javascript src="textAngular.min.js"></g:javascript>
<g:javascript src="textAngular-sanitize.min.js"></g:javascript>
<g:javascript src="../metisMenu/jquery.metisMenu.js"></g:javascript>
<g:javascript src="../bootstrap/js/sb-admin.js"></g:javascript>
<g:javascript src="yvalidator.js"></g:javascript>

<script>
    var contextPath = '${request.contextPath}';
    var addMessage = '${g.message(code: "add", default: "Add")}';
    var editMessage = '${g.message(code: "edit", default: "Edit")}';
    var deleteMessage = '${g.message(code: "delete", default: "Delete {0} ?")}';
    var setLeaderMessage = '${g.message(code: "setLeader", default: "Set {0} as leader?")}';
    var removeLeaderMessage = '${g.message(code: "removeLeader", default: "Remove {0} leader?")}';
</script>
<g:javascript src="admin.js"></g:javascript>

</body>

</html>
