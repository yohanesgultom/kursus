<style>
.overlay {
    background:rgba(255,255,255,.75);
    text-align:center;
    height:100%;
    opacity:0;
    -webkit-transition: opacity .25s ease;
    padding:50px;
}

.module:hover .overlay {
    opacity:1;
}

.overlay .glyphicon:hover {
    cursor: pointer;
}

.overlay .glyphicon {
    vertical-align: middle;
    padding: 15px;
    color:rgba(50,50,50,.85);
    font-size:48px;
}

</style>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><g:message code="kursus.admin.module" default="Module"></g:message></h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <span class="btn btn-success btn-lg" ng-click="add()"><i class="glyphicon glyphicon-plus"></i> <g:message code="add" default="Add"></g:message></span>
    </div>
</div>

<br>

<div class="row">
    <g:img file="ajax-loader.gif" ng-show="loading"></g:img>
    <div ng-repeat="module in modules" class="module col-md-4 col-sm-6 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-thumbnail" ng-bg="{{module.cover}}">
                <div class="overlay">
                    <i class="glyphicon glyphicon-th-list" ng-click="topicList(module.id,module.name)"></i>
                    <i class="glyphicon glyphicon-pencil" ng-click="edit(module.id)"></i>
                    <i class="glyphicon glyphicon-remove" ng-click="delete(module.id,module.name)"></i>
                </div>
            </div>
            <div class="panel-body">
                <h3>{{module.name}}</h3>
                <p class="description">{{module.description}}</p>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="moduleModal" tabindex="-1" role="dialog" aria-labelledby="moduleModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="moduleModalLabel"></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="moduleId" ng-model="module.id">
                <div class="form-group">
                    <label for="moduleName"><g:message code="kursus.module.name" default="Name"/></label>
                    <input type="text" class="form-control" id="moduleName" ng-model="module.name">
                </div>
                <div class="form-group">
                    <label for="moduleImageUrl"><g:message code="kursus.module.imageurl" default="Image URL"/></label>
                    <input type="text" class="form-control" id="moduleImageUrl" ng-model="module.cover">
                </div>
                <div class="form-group">
                    <label for="moduleDesc"><g:message code="kursus.module.desc" default="Description"/></label>
                    <textarea class="form-control" id="moduleDesc" rows="12" ng-model="module.description"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message code="cancel" default="Cancel"/></button>
                <button type="button" class="btn btn-primary" ng-click="save()"><g:message code="save" default="Save"/></button>
            </div>
        </div>
    </div>
</div>

<!-- topics list modal -->
<div class="modal fade" id="topicsModal" tabindex="-1" role="dialog" aria-labelledby="moduleModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="topicsModalLabel"></h4>
            </div>
            <div class="modal-body">
                <span class="btn btn-success" ng-click="topicAdd()">
                    <i class="glyphicon glyphicon-plus"></i> <g:message code="add" default="Add"></g:message>
                </span>
                <br><br>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" ng-model="topicSearch.title" placeholder="${g.message(code:'kursus.topic.searchplaceholder',default:'Search topic title')}">
                    <span class="glyphicon glyphicon-search form-control-feedback" style="top:1px"></span>
                </div>
                <g:img file="ajax-loader.gif" ng-show="topicsLoading"></g:img>
                <div class="list-group">
                    <div class="topic list-group-item" ng-repeat="topic in topics | mapfilter:topicSearch">
                        <div class="action pull-right">
                            <span class="btn btn-info" title="edit" ng-click="topicEdit(topic.id)">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </span>
                            <span class="btn btn-danger" title="delete" ng-click="topicDelete(topic.id,topic.title)">
                                <i class="glyphicon glyphicon-remove"></i>
                            </span>
                        </div>
                        <h4>{{topic.title}}</h4>
                        <p ng-bind-html="topic.body"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- topic edit modal -->
<div class="modal fade" id="topicEditModal" tabindex="-1" role="dialog" aria-labelledby="moduleModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" ng-click="topicClose()">&times;</button>
                <h4 class="modal-title" id="topicEditLabel"></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="topicId" ng-model="topic.id">
                <div class="form-group">
                    <label><g:message code="kursus.topic.title" default="Title"/></label>
                    <input type="text" class="form-control" ng-model="topic.title">
                </div>
                <div class="form-group">
                    <label><g:message code="kursus.topic.body" default="Body"/></label>
                    <div text-angular="text-angular" ng-model="topic.body"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" ng-click="topicClose()"><g:message code="cancel" default="Cancel"/></button>
                <button type="button" class="btn btn-primary" ng-click="topicSave()"><g:message code="save" default="Save"/></button>
            </div>
        </div>
    </div>
</div>
