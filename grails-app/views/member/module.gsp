<html>
<head>
    <meta name="layout" content="member">
    <title>${module.name}</title>
    <r:style>
        .response p {
            color: grey;
        }

        .response {
            display: none;
        }

        .response.self {
            display: block;
        }

        .response.self p {
            color: green;
        }
    </r:style>
</head>
<body>

<div class="col-md-12 col-sm-12">
    <div class="panel panel-default well">
        <h4>${module.name}</h4>
        <div class="panel-body">
            <div class="media">
                <img class="thumbnail pull-left" src="${module.cover}" height="250px">
                </span>
                <p>${module.description}</p>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 col-sm-12">
    <div class="panel panel-default well">
        <div class="row">
            <div class="col-lg-9">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control input-lg" ng-model="topicSearch.title" placeholder="${g.message(code:'kursus.topic.searchplaceholder',default:'Search topic title')}">
                    <span class="glyphicon glyphicon-search form-control-feedback" style="top:1px"></span>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <input type="checkbox" ng-model="topicSearch.responded" ng-true-value="false" ng-false-value=""> <g:message code="kursus.topic.responded" default="Not yet responded"></g:message>
                </div>
            </div>
        </div>
    </div>
</div>


<g:img file="ajax-loader.gif" ng-show="loading"></g:img>
<div id="{{topic.id}}" class="col-md-12 col-sm-12" ng-repeat="topic in moduleTopics | andmapfilter:topicSearch">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="media topic">
                <button class="btn btn-warning pull-right" type="button" ng-click="toggleView(topic.id)" title="<g:message code='kursus.viewallresponses' default='Show All Response(s)'/>"><i class="glyphicon glyphicon-comment"></i></button>
                <h4>{{topic.title}}</h4>
                <p ng-bind-html="topic.body"></p>
            </div>
            <div class="media response{{(response.self) ? ' self' : ''}}" ng-repeat="response in topic.responses">
                <div class="pull-left" style="margin-bottom: 7px">
                    <img class="thumbnail" style="margin-bottom:3px" ng-src="{{response.useravatar}}" width="50px" height="50px" title="{{response.username}}"/>
                    <div style="font-size: 7pt; text-align: center;">{{response.username}}</div>
                </div>
                <p ng-bind-html="response.body"></p>
            </div>
            <div class="response-input">
                <textarea style="width:100%" ng-model="newResponse[topic.id]"></textarea>
                <button class="btn btn-success pull-right" type="button" ng-click="saveResponse(topic.id, topic.responded)" title="<g:message code='kursus.addresponse' default='Add Response'/>" ng-disabled="disabledButton[topic.id]"><g:message code='kursus.save' default='Save'/></button>
            </div>
        </div>
    </div>
</div>

<r:script>
angular.module('app',['ngSanitize','ngCustomFilters']).
        controller('controller', function ($scope, $http) {
            $scope.loading = true;
            $http.get('${request.contextPath}/member/moduleTopics/'+${module.id}).success(function(data) {
                $scope.moduleTopics = data.topics;
                $scope.loading = false;
            });

            $scope.viewAll = {};
            $scope.toggleView = function(topicId) {
                var container = $('#'+topicId);
                if ($scope.viewAll[topicId]) {
                    $scope.viewAll[topicId] = false;
                    $('.response', container).not('.self').slideUp();
                } else {
                    $scope.viewAll[topicId] = true;
                    $('.response', container).not('.self').slideDown();
                }
            }

            $scope.newResponse = {};
            $scope.disabledButton = {}
            $scope.saveResponse = function(topicId, responded) {
                var message, url, data = null;
                if (responded) {
                    url = '${request.contextPath}/member/updateResponse';
                    message = '${g.message(code:"confirmation.updateresponse", default: "Update response?")}';
                } else {
                    url = '${request.contextPath}/member/addResponse';
                    message = '${g.message(code:"confirmation.addresponse", default: "Add response?")}';
                }
                if (confirm(message)) {
                    data = {topicId: topicId, body: $scope.newResponse[topicId]};
                    $scope.disabledButton[topicId] = true;
                    $http.post(url, {}, {params: data}).success(function(res) {
                        try {
                            if (responded) {
                                $scope.moduleTopics[topicId]['responses'][res.response.id]['body'] = res.response.body;
                            } else {
                                $scope.moduleTopics[topicId]['responses'][res.response.id] = res.response;
                                $scope.moduleTopics[topicId].responded = true;
                            }
                            $scope.newResponse[topicId] = '';
                            //alert(res.message);
                        } catch (e) {
                            alert(e);
                        }
                        $scope.disabledButton[topicId] = false;
                    });
                }
            }

        });
</r:script>

</body>
</html>