<html>
<head>
    <meta name="layout" content="member">
    <title><g:message code="kursus.home" default="Home"/></title>
</head>
<r:style>
.overlay {
    background:rgba(255,255,255,.75);
    text-align:center;
    height:100%;
    opacity:0;
    -webkit-transition: opacity .25s ease;
    padding:50px;
}

.userModule:hover .overlay {
    opacity:1;
}

.plus {
   vertical-align: middle;
   color:rgba(50,50,50,.85);
   font-size:72px;
}

.stat {
    font-size: 18px;
    padding: 5px;
}

.modal-body label {
    font-weight:normal;
    color:#888;
}
</r:style>
<body>

<div class="col-md-12 col-sm-12">
    <div class="panel panel-default well">
        <h4><g:message code="kursus.myprofile" default="My Profile"/></h4>
        <div class="panel-body">
            <div class="media">
                <a class="thumbnail pull-left" href="#" data-toggle="modal" data-target="#profileModal">
                    <img ng-src="{{me.avatar}}" class="media-object" width="100px" height="100px"/>
                </a>
                <div class="media-body">
                    <p><g:message code="kursus.hello" default="Hello, "/>{{me.fullname}}</p>
                    <p>
                        <span class="label label-info">{{me.job}}</span>
                        <span class="label label-primary">{{me.address}}</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<g:img file="ajax-loader.gif" ng-show="loading"></g:img>
<div ng-repeat="userModule in userModules" class="userModule col-md-4 col-sm-6 col-xs-12">
    <div class="panel panel-default">
        <div class="panel-thumbnail" ng-bg="{{userModule.cover}}">
            <div class="overlay" ng-click="action(userModule.subscribed, userModule.id)">
                <i class="glyphicon glyphicon-plus plus" ng-hide="userModule.subscribed"></i>
                <i class="glyphicon glyphicon-ok plus" ng-show="userModule.subscribed"></i>
            </div>
        </div>
        <div class="panel-body">
            <p class="lead">{{userModule.name}}</p>
            <div class="progress progress-striped" ng-show="userModule.subscribed">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{userModule.percentage}}" aria-valuemin="0" aria-valuemax="100" style="{{'width:' + userModule.percentage + '%'}}">
                    <span>{{userModule.percentage}}% <g:message code="complete" default="Complete"/></span>
                </div>
            </div>
            <p class="description">{{userModule.description}}</p>
        </div>
    </div>
</div>

<!-- Profile Edit Modal -->
<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="lead"><g:message code="myprofile" default="My Profile"/></div>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label id="userEmail"><g:message code="kursus.user.email" default="E-mail"/></label>
                    <input type="text" class="form-control" ng-model="user.username" disabled readonly>
                </div>
                <div class="form-group">
                    <label id="userFullName"><g:message code="kursus.user.fullname" default="Full Name"/> * </label>
                    <input type="text" class="form-control" ng-model="user.fullname">
                </div>
                <div class="form-group">
                    <div class="radio-inline">
                        <label><input type="radio" name="gender" value="{{genders.male}}" ng-model="user.gender" checked> <g:message code="male" default="Male"/></label>
                    </div>
                    <div class="radio-inline">
                        <label><input type="radio" name="gender" value="{{genders.female}}" ng-model="user.gender"> <g:message code="female" default="Female"/></label>
                    </div>
                </div>
                <div class="form-group">
                    <label id="userAvatar"><g:message code="kursus.user.avatar" default="Avatar"/></label>
                    <input type="text" class="form-control" ng-model="user.avatar">
                </div>
                <div class="form-group">
                    <label id="userJob"><g:message code="kursus.user.job" default="Job"/></label>
                    <input type="text" class="form-control" ng-model="user.job">
                </div>
                <div class="form-group">
                    <label id="userAddress"><g:message code="kursus.user.address" default="Address"/></label>
                    <input type="text" class="form-control" ng-model="user.address">
                </div>

                <!-- change password -->
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <a data-toggle="collapse" href="#change-password">
                            <g:message code="changepassword" default="Change Password"></g:message>
                        </a>
                    </div>
                    <div id="change-password" class="panel-body panel-collapse collapse">
                        <div class="form-group">
                            <label id="userCurrentPassword"><g:message code="kursus.user.currentpassword" default="Current Password"/></label>
                            <input type="password" class="form-control" ng-model="password.current">
                        </div>
                        <div class="form-group">
                            <label id="userNewPassword"><g:message code="kursus.user.newpassword" default="New Password"/></label>
                            <input type="password" class="form-control" ng-model="password.new">
                        </div>
                        <div class="form-group">
                            <label id="userConfirmPassword"><g:message code="kursus.user.passwordconfirm" default="Confirm Password"/></label>
                            <input type="password" class="form-control" ng-model="password.confirm">
                        </div>
                        <div class="btn btn-warning" ng-click="changePassword()"><g:message code="changepassword" default="Change Password"></g:message></div>
                    </div>
                </div
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" ng-disabled="disabled"><g:message code="default.button.cancel.label" default="Cancel"/></button>
                <button type="button" class="btn btn-primary" ng-click="saveProfile()" ng-disabled="disabled"><g:message code="default.button.save.label" default="Save"/></button>
            </div>
        </div>
    </div>
</div>

<r:script>
var contextPath = '${request.contextPath}';
var profileModal = $('#profileModal');
var passwordChangedMessage = '${g.message(code:"password.change.success", default:"Password is successfully changed")}';
var defaultErrorMessage = '${g.message(code:"default.error.message", default:"Server error. Please try again")}';
var changePasswordConfirmationMsg = '${g.message(code:"password.change.confirm", default:"Are you sure to change password ?")}';

var app = angular.module('app',[])

.config(function($httpProvider) {
    // important! to make sure server receive the params
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) { return data;}
        return $.param(data);
    }
})

.controller('controller', function ($scope, $http) {

    $scope.disabled = false;
    $scope.genders = {male:'M',female:'F'};

    $scope.loadProfile = function() {
        $http.get(contextPath + '/member/loadProfile').success(function(data) {
            $scope.user = data.user;
            $scope.me = angular.copy(data.user);
        });
    }

    $scope.loadUserModules = function() {
        $scope.loading = true;
        $http.get(contextPath + '/member/userModules').success(function(data) {
            $scope.userModules = data.modules;
            $scope.loading = false;
        });
    }

    $scope.action = function(open, moduleId) {
        if (open) {
            window.open(contextPath + '/member/module/'+moduleId, '_self');
        } else {
            var params = {moduleId: moduleId};
            $http.post(contextPath + '/member/subscribeModule', {}, {params: params}).success(function(res) {
                if (res && res.success) {
                    $scope.userModules[moduleId].subscribed = true;
                }
                alert(res.message);
            });
        }
    }

    $scope.saveProfile = function() {
        $scope.disabled = true;
        var valid = yvalidator.validate([
            {
                valid: ($scope.user.fullname),
                target: '#userFullName',
                message: 'Please provide full name'
            }
        ]);

        if (valid) {
            $http.post(contextPath + '/member/saveProfile', $scope.user).success(function(res) {
                if (res.success == true) {
                    $scope.me = angular.copy(res.user);
                    profileModal.modal('hide');
                } else {
                    alert(res.message);
                }
                $scope.disabled = false;
            });
        } else {
            $scope.disabled = false;
        }
    }

    $scope.changePassword = function() {
        if (confirm(changePasswordConfirmationMsg)) {
            $scope.disabled = true;
            var valid = yvalidator.validate([
                {
                    valid: ($scope.password.current),
                    target: '#userCurrentPassword',
                    message: 'Please provide old password'
                },
                {
                    valid: ($scope.password.new),
                    target: '#userNewPassword',
                    message: 'Please provide new password'
                },
                {
                    valid: (!$scope.password.new || $scope.password.new.length >= 6),
                    target: '#userNewPassword',
                    message: 'Password minimum length is 6'
                },
                {
                    valid: (!$scope.password.confirm || $scope.password.confirm == $scope.password.new),
                    target: '#userConfirmPassword',
                    message: 'Password confirmation is different'
                }
            ]);

            if (valid) {
                $http.post(contextPath + '/member/changePassword', $scope.password).success(function(res) {
                    if (res.success == true) {
                        $scope.password = null;
                        alert(passwordChangedMessage);
                    } else {
                        alert(res.message);
                    }
                    $scope.disabled = false;
                });
            }
        }
    }

    // init
    $scope.loadProfile();
    $scope.loadUserModules();
})

.directive('ngBg', function(){
    return function(scope, element, attrs){
        var url = attrs.ngBg;
        element.css({
            'background-image': 'url(' + url +')',
            'background-size' : 'cover'
        });
    };
});
</r:script>

</body>
</html>