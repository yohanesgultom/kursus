import grails.util.Environment
import kursus.KursusModule
import kursus.KursusRole
import kursus.KursusUser
import kursus.KursusUserGroup
import kursus.Response
import kursus.Topic
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

    def init = { servletContext ->

        def currentEnv = Environment.current

        if (currentEnv == Environment.DEVELOPMENT) {

            def adminRole = new KursusRole(name: KursusRole.Roles.Admin)
            adminRole.addToPermissions("*:*")
            adminRole.save(failOnError: true)

            def memberRole = new KursusRole(name: KursusRole.Roles.Member)
            memberRole.addToPermissions("member:*")
            memberRole.save(failOnError: true)

            def leaderRole = new KursusRole(name: KursusRole.Roles.Leader)
            leaderRole.addToPermissions("member:*")
            leaderRole.addToPermissions("leader:*")
            leaderRole.save(failOnError: true)

            def group1 = new KursusUserGroup(name: "Alpha").save()
            def group2 = new KursusUserGroup(name: "Bravo").save()
            def group3 = new KursusUserGroup(name: "Charlie").save()

            def admin = new KursusUser(username: "admin@gmail.com", fullname: "Administrator", gender: KursusUser.GENDERS.MALE, passwordHash: new Sha256Hash("password").toHex(), address: "Jakarta", job: "Web administrator", avatar: "http://findicons.com/files/icons/1072/face_avatars/300/f01.png")
            admin.addToRoles(adminRole)
            admin.save(failOnError: true)

            def user = new KursusUser(username: "user@gmail.com", fullname: "Leader", gender: KursusUser.GENDERS.FEMALE, passwordHash: new Sha256Hash("password").toHex(), address: "Bali", job: "Doctor", avatar: "http://findicons.com/files/icons/1072/face_avatars/300/fc05.png", kursusUserGroup: group1)
            user.addToRoles(leaderRole)
            user.save(failOnError: true)

            def user2 = new KursusUser(username: "user2@gmail.com", fullname: "User2", gender: KursusUser.GENDERS.MALE, passwordHash: new Sha256Hash("password").toHex(), address: "Yogyakarta", job: "Pilot", avatar: "http://icons.iconarchive.com/icons/hopstarter/face-avatars/256/Male-Face-H5-icon.png", kursusUserGroup: group1)
            user2.addToRoles(memberRole)
            user2.save(failOnError: true)

            def kerasulanEkonomi = new KursusModule(name: "Kerasulan Ekonomi", description: "Modul Pendalaman Alkitab pelayanan Pakars Indonesia", cover: "http://www.pakarsindonesia.org/images/BecomingTheEconomicApostle.JPG")
            kerasulanEkonomi.addToSubscribers(admin)
            kerasulanEkonomi.addToSubscribers(user)
            kerasulanEkonomi.addToSubscribers(user2)
            kerasulanEkonomi.save()

            def topic1 = new Topic(title: "Pengelolaan Keuangan: Renungan Hari Pertama", body: "<p>Bacaan: Keluaran 9:29; Yesaya 66:1-2, Mazmur 89:12, Wahyu 4:11</p><p>Menurut bacaan di atas apa yang diajarkan tentang kepemilikan langit dan bumi?</p>", kursusModule: kerasulanEkonomi).save()
            def topic2 = new Topic(title: "Pengelolaan Keuangan: Renungan Hari Kedua", body: "<p>Bacaan: 1 Timotius 6:17; Hagai 2:9; Mazmur 50:10-12</p><p>Menurut bacaan di atas dari manakah harta milik kita dan bagaimanakah harta milik itu ada pada kita?</p>", kursusModule: kerasulanEkonomi).save()

            new Response(body: "Langit dan bumi dimiliki oleh penciptanya sendiri yaitu Allah", topic: topic1, user: admin).save()
            new Response(body: "Dimiliki oleh Allah", topic: topic1, user: user).save()
            new Response(body: "Semuanya dimiliki oleh Tuhan", topic: topic1, user: user2).save()

            def bertumbuhDalamKristus = new KursusModule(name: "Bertumbuh dalam Kristus", description: "Modul Pendalaman Alkitab dasar", cover: "http://4.bp.blogspot.com/-1qjZLpf0ESk/UNcXlnT9qVI/AAAAAAAAANY/xENGQvf3mmY/s1600/bertumbuh.jpg")
            bertumbuhDalamKristus.save()

        } else if (currentEnv == Environment.PRODUCTION) {

            if (KursusRole.all.empty) {
                def adminRole = new KursusRole(name: KursusRole.Roles.Admin)
                adminRole.addToPermissions("*:*")
                adminRole.save(flush:true)

                def memberRole = new KursusRole(name: KursusRole.Roles.Member)
                memberRole.addToPermissions("member:*")
                memberRole.save(flush: true)

                def leaderRole = new KursusRole(name: KursusRole.Roles.Leader)
                leaderRole.addToPermissions("member:*")
                leaderRole.addToPermissions("leader:*")
                leaderRole.save(flush: true)
            }

            if (KursusUserGroup.all.empty) {
                def group1 = new KursusUserGroup(name: "Test Group").save(flush: true)
            }

            if (KursusUser.all.empty) {
                def adminRole = KursusRole.findByName(KursusRole.Roles.Admin)
                def leaderRole = KursusRole.findByName(KursusRole.Roles.Leader)
                def memberRole = KursusRole.findByName(KursusRole.Roles.Member)
                def group1 = KursusUserGroup.first()

                def admin = new KursusUser(username: "admin@gmail.com", fullname: "Administrator", gender: KursusUser.GENDERS.MALE, passwordHash: new Sha256Hash("password").toHex(), address: "Jakarta", job: "Web administrator", avatar: "http://findicons.com/files/icons/1072/face_avatars/300/f01.png")
                admin.addToRoles(adminRole)
                admin.save(flush: true)

                def user = new KursusUser(username: "user@gmail.com", fullname: "Leader", gender: KursusUser.GENDERS.FEMALE, passwordHash: new Sha256Hash("password").toHex(), address: "Bali", job: "Doctor", avatar: "http://findicons.com/files/icons/1072/face_avatars/300/fc05.png", kursusUserGroup: group1)
                user.addToRoles(leaderRole)
                user.save(flush: true)

                def user2 = new KursusUser(username: "user2@gmail.com", fullname: "User2", gender: KursusUser.GENDERS.MALE, passwordHash: new Sha256Hash("password").toHex(), address: "Yogyakarta", job: "Pilot", avatar: "http://icons.iconarchive.com/icons/hopstarter/face-avatars/256/Male-Face-H5-icon.png", kursusUserGroup: group1)
                user2.addToRoles(memberRole)
                user2.save(flush: true)
            }

            if (KursusModule.all.empty) {
                def kerasulanEkonomi = new KursusModule(name: "Kerasulan Ekonomi", description: "Modul Pendalaman Alkitab pelayanan Pakars Indonesia", cover: "http://www.pakarsindonesia.org/images/BecomingTheEconomicApostle.JPG")
                kerasulanEkonomi.save()

                def topic1 = new Topic(title: "Pengelolaan Keuangan: Renungan Hari Pertama", body: "<p>Bacaan: Keluaran 9:29; Yesaya 66:1-2, Mazmur 89:12, Wahyu 4:11</p><p>Menurut bacaan di atas apa yang diajarkan tentang kepemilikan langit dan bumi?</p>", kursusModule: kerasulanEkonomi).save()
                def topic2 = new Topic(title: "Pengelolaan Keuangan: Renungan Hari Kedua", body: "<p>Bacaan: 1 Timotius 6:17; Hagai 2:9; Mazmur 50:10-12</p><p>Menurut bacaan di atas dari manakah harta milik kita dan bagaimanakah harta milik itu ada pada kita?</p>", kursusModule: kerasulanEkonomi).save()
            }
        }

    }

    def destroy = {
    }
}
