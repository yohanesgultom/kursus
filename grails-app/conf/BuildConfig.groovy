grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
//grails.project.war.file = "target/${appName}-${appVersion}.war"

// uncomment (and adjust settings) to fork the JVM to isolate classpaths
//grails.project.fork = [
//   run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
//]

grails.project.dependency.resolution = {
    inherits("global") {
        excludes 'xercesImpl', 'xml-apis'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve

    repositories {
        inherits true // Whether to inherit repository definitions from plugins
        grailsPlugins()
        grailsHome()
        grailsCentral()
        mavenCentral()
        mavenLocal()
        mavenRepo "http://maven.touk.pl/nexus/content/repositories/releases"
    }
    dependencies {
        runtime 'postgresql:postgresql:8.4-702.jdbc3'
    }

    plugins {
        compile ":hibernate:$grailsVersion"
        runtime ":resources:1.2.7"
        compile ":shiro:1.2.1"
        compile ":cache:1.0.1"
        compile ":shiro-oauth:0.3"
        build ":tomcat:$grailsVersion"
    }
}
