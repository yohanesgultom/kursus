class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(controller: 'member', action: 'home')
		"500"(view:'/error')
        "404"(view:'/404')

        "/oauth/success"(controller: "shiroOAuth", action: "onSuccess")
        "/oauth/callback/$provider"(controller: "oauth", action: "callback")
	}
}
